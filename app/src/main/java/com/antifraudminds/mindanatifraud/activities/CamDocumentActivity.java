package com.antifraudminds.mindanatifraud.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.antifraudminds.mindanatifraud.R;
import com.google.zxing.common.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by kikeval on 8/09/16.
 */
public class CamDocumentActivity extends AppCompatActivity {
    public static final int ACTIVITY_START_CAMERA_APP = 0;
    public static final int ACTIVITY_START_CAMERA_APP_RETIRO = 1;

    public static final int GET_FOTOS_CEDULA = 3714;
    public static final String IMAGE_TIRO = "imageTiro";
    public static final String IMAGE_RETIRO = "imageRetiro";
    public static final String IMAGE_TIRO_URI = "imageTiroUri";
    public static final String IMAGE_RETIRO_URI = "imageRetiroUri";
    public static final String DATA = "data";

    @BindView(R.id.imgDocTiro)
    ImageView imagePhotoDocumentTiro;
    @BindView(R.id.imgDocRetiro)
    ImageView imagePhotoDocumentRetiro;
    @BindView(R.id.btnTiro)
    Button botonTiro;
    @BindView(R.id.btnRetiro)
    Button botonRetiro;
    @BindView(R.id.vincularImagenes)
    Button botonVincularImg;
    @BindView(R.id.contentTiro)
    LinearLayout contentTiro;
    @BindView(R.id.retiroContent)
    LinearLayout contentRetiro;

    private String uriTiro;
    private String uriRetiro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setResult(RESULT_CANCELED);
        setContentView(R.layout.activity_cam_document);
        ButterKnife.bind(this);
        botonVincularImg.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               if (uriTiro == null && uriRetiro == null){
                   new AlertDialog.Builder(CamDocumentActivity.this)
                           .setTitle("Vincular Imagenes Documento")
                           .setMessage("Debe tomar las imagenes con la camara de la aplicación para poder vincular las imagenes al formulario.")
                           .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                               public void onClick(DialogInterface dialog, int which) {
                                   Log.d("AlertDialog", "Positive");
                               }
                           })
                           .show();
               } else {
                   setIntentResult();
                   finish();
               }
            }
        });
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState.containsKey(IMAGE_TIRO_URI)) {
            uriTiro = savedInstanceState.getString(IMAGE_TIRO_URI);
            Bitmap capturaBitMap = null;
            try {
                capturaBitMap = ValidarActivity.getBitmapFromFile(uriTiro);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            if (capturaBitMap != null) {
                imagePhotoDocumentTiro.setVisibility(View.VISIBLE);
                imagePhotoDocumentTiro.setImageBitmap(capturaBitMap);
                contentTiro.setVisibility(View.GONE);
            }
        }

        if (savedInstanceState.containsKey(IMAGE_RETIRO_URI)) {
            uriRetiro = savedInstanceState.getString(IMAGE_RETIRO_URI);
            Bitmap capturaBitMapDos = null;
            try {
                capturaBitMapDos = ValidarActivity.getBitmapFromFile(uriTiro);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            if (capturaBitMapDos != null) {
                imagePhotoDocumentRetiro.setVisibility(View.VISIBLE);
                imagePhotoDocumentRetiro.setImageBitmap(capturaBitMapDos);
                contentRetiro.setVisibility(View.GONE);
            }
        }
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override protected void onSaveInstanceState(Bundle outState) {
        outState.putString(IMAGE_TIRO_URI, uriTiro);
        outState.putString(IMAGE_RETIRO_URI, uriRetiro);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStop() {
        setIntentResult();
        super.onStop();

    }

    private void setIntentResult() {
        if (uriTiro != null && uriRetiro != null) {
            Intent data = new Intent();
            data.putExtra(IMAGE_TIRO_URI, uriTiro);
            data.putExtra(IMAGE_RETIRO_URI, uriRetiro);
            setResult(RESULT_OK, data);
        }
    }

    public void takeDocumentTiro(View view){
        Intent intentCamaraAppTiro = new Intent();
        intentCamaraAppTiro.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
        uriTiro = createFileName(null);
        Uri uriFile;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            uriFile = getFileUriProvider(this, uriTiro);
            intentCamaraAppTiro.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        } else {
            uriFile = getFileUri(uriTiro);
        }
        intentCamaraAppTiro.putExtra(MediaStore.EXTRA_OUTPUT, uriFile);
        startActivityForResult(intentCamaraAppTiro , ACTIVITY_START_CAMERA_APP);
    }
    public void takeDocumentRetiro(View view){
        Intent intentCamaraAppRetiro = new Intent();
        intentCamaraAppRetiro.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
        uriRetiro = createFileName(null);
        Uri uriFile;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            uriFile = getFileUriProvider(this, uriRetiro);
            intentCamaraAppRetiro.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        } else {
            uriFile = getFileUri(uriRetiro);
        }
        intentCamaraAppRetiro.putExtra(MediaStore.EXTRA_OUTPUT, uriFile);
        startActivityForResult(intentCamaraAppRetiro , ACTIVITY_START_CAMERA_APP_RETIRO);
    }

    public static String createFileName(String prefix) {
        prefix = prefix == null ? "fname_" : prefix;
        return prefix + UUID.randomUUID().toString() + ".jpg";
    }

    public static Uri getFileUriProvider(Context context, String filename) {
        File file = new File(Environment.getExternalStorageDirectory(), filename);
        return FileProvider.getUriForFile(context, "com.antifraudminds.mindanatifraud.provider", file);
    }

    public static Uri getFileUri(String filename) {
        File file = new File(Environment.getExternalStorageDirectory(), filename);
        return Uri.fromFile(file);
    }

// show data to camera
// show animate items biometric Image user

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
        if (resultCode == RESULT_OK) {
            if (requestCode == ACTIVITY_START_CAMERA_APP) {

                try {
                    Bitmap capturaBitMap = ValidarActivity.getBitmapFromFile(uriTiro);
                    imagePhotoDocumentTiro.setVisibility(View.VISIBLE);
                    imagePhotoDocumentTiro.setImageBitmap(capturaBitMap);
                    imagePhotoDocumentTiro.invalidate();
                    contentTiro.setVisibility(View.GONE);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            if (requestCode == ACTIVITY_START_CAMERA_APP_RETIRO) {
                try {
                    Bitmap capturaBitMapDos = ValidarActivity.getBitmapFromFile(uriRetiro);
                    imagePhotoDocumentRetiro.setVisibility(View.VISIBLE);
                    imagePhotoDocumentRetiro.setImageBitmap(capturaBitMapDos);
                    contentRetiro.setVisibility(View.GONE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}







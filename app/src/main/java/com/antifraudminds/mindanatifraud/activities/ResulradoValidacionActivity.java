package com.antifraudminds.mindanatifraud.activities;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.antifraudminds.mindanatifraud.R;
import com.antifraudminds.mindanatifraud.model.ConsultaResponse;
import com.antifraudminds.mindanatifraud.model.DecrimData;
import com.antifraudminds.mindanatifraud.model.DecrimRequestSearchByIdCaso;
import com.antifraudminds.mindanatifraud.model.DecrimResponseEnvelopeSearchById;
import com.antifraudminds.mindanatifraud.model.ListaNegraData;
import com.antifraudminds.mindanatifraud.model.PdfFileRequest;
import com.antifraudminds.mindanatifraud.model.PerfilPersona;
import com.antifraudminds.mindanatifraud.model.ResponseManager;
import com.antifraudminds.mindanatifraud.model.ResponseManagerDecrimData;
import com.antifraudminds.mindanatifraud.model.ResponseManagerListaNegra;
import com.antifraudminds.mindanatifraud.model.ResponseManagerPdfGenerate;
import com.antifraudminds.mindanatifraud.model.ServiceCommunication;
import com.antifraudminds.mindanatifraud.model.Usuario;
import com.antifraudminds.mindanatifraud.views.LoadingDialog;
import com.hookedonplay.decoviewlib.DecoView;
import com.hookedonplay.decoviewlib.charts.SeriesItem;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kikeval on 2/09/16.
 */
public class ResulradoValidacionActivity extends AppCompatActivity {


    @BindView(R.id.lblNombreCompleto)
    TextView lblNombreCompleto;
    @BindView(R.id.lblNumDocumento)
    TextView lblNumDocumento;
    @BindView(R.id.imgUsuario)
    CircularImageView imgUsuario;

    @BindView(R.id.dynamicArcView)
    DecoView decoView;
    @BindView(R.id.textPercentage)
    TextView textPercentage;


    @BindView(R.id.lblAnalisisGeneral)
    TextView lblAnalisisGeneral;
    @BindView(R.id.imgAnalisisGeneral)
    ImageView imgAnalisisGeneral;

    @BindView(R.id.lblAnalisisDocumento)
    TextView lblAnalisisDocumento;
    @BindView(R.id.imgAnalisisDocumento)
    ImageView imgAnalisisDocumento;

    @BindView(R.id.lblAnalisisHuella)
    TextView lblAnalisisHuella;
    @BindView(R.id.imgAnalisisHuella)
    ImageView imgAnalisisHuella;

    @BindView(R.id.lblAnalisisBasesDeDatos)
    TextView lblAnalisisBasesDeDatos;
    @BindView(R.id.imgAnalisisBasesDeDatos)
    ImageView imgAnalisisBasesDeDatos;
    @BindView(R.id.btnDownload)
    Button btnDownload;

    @BindView(R.id.containerAnalisisGeneral)
    View containerAnalisisGeneral;
    @BindView(R.id.containerAnalisisDocumento)
    View containerAnalisisDocumento;
    @BindView(R.id.containerAnalisisHuella)
    View containerAnalisisHuella;
    @BindView(R.id.containerAnalisisBasesDeDatos)
    View containerAnalisisBasesDeDatos;

    private String idCaso;
    private String imgUsuarioData;
    private Usuario usuario;

    private List<ConsultaResponse> consultaResponseList;
    private List<ListaNegraData> listaNegraDataList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado_validacion);
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (getIntent().hasExtra(HistorialActivity.KEY_ID_CASO) && getIntent().hasExtra(HistorialActivity.KEY_IMAGE)) {
            LoadingDialog.showDialog(this);

            idCaso = getIntent().getStringExtra(HistorialActivity.KEY_ID_CASO);
            imgUsuarioData = getIntent().getStringExtra(HistorialActivity.KEY_IMAGE);
            LoadingDialog.updateText("Obteniendo Datos de Validación del Caso " + idCaso);
            try {
                usuario = Usuario.retrieveUsuarioLoginData(getSharedPreferences(Usuario.KEY_USUARIO, MODE_PRIVATE));
            } catch (IOException e) {
                e.printStackTrace();
            }

            switch (usuario.getTipo()) {
                case 1:
                    containerAnalisisGeneral.setVisibility(View.VISIBLE);
                    containerAnalisisDocumento.setVisibility(View.VISIBLE);
                    containerAnalisisHuella.setVisibility(View.VISIBLE);
                    containerAnalisisBasesDeDatos.setVisibility(View.VISIBLE);
                    getCaso();
                    break;
                case 2:
                    containerAnalisisGeneral.setVisibility(View.GONE);
                    containerAnalisisDocumento.setVisibility(View.GONE);
                    containerAnalisisHuella.setVisibility(View.GONE);
                    containerAnalisisBasesDeDatos.setVisibility(View.VISIBLE);
                    getDataByIdCaso(idCaso);
                    break;
                default:
            }
        }
    }

    private void getDataByIdCaso(String idCaso) {
        DecrimData.getByIdCaso(idCaso, new Callback<ResponseManagerDecrimData>() {
            @Override
            public void onResponse(Call<ResponseManagerDecrimData> call,
                Response<ResponseManagerDecrimData> response) {
                if (response != null && response.body() != null) {
                    DecrimData decrimData = response.body().getObject().get(0);
                    ConsultaResponse consultaResponse = new ConsultaResponse();
                    consultaResponse.setpNombre(decrimData.getNombres());
                    consultaResponse.setsNombre("");
                    consultaResponse.setpApellido(decrimData.getApellidos());
                    consultaResponse.setsApellido("");
                    consultaResponse.setNumeroIdentificacion(decrimData.getNumDocumento());
                    busquedaListaNegra(consultaResponse);
                }
            }

            @Override
            public void onFailure(Call<ResponseManagerDecrimData> call, Throwable t) {
                String error = t.getMessage();
            }
        });
    }

    /**
     * Setea el valor del porcentaje del resultado.
     *
     * @param percentage valor del porcentaje.
     */
    private void setPercentData(float percentage) {
        int color = getColorPercentage(percentage);
        decoView.addSeries(new SeriesItem.Builder(color)
                .setShadowColor(Color.argb(128, 218, 218, 218))
                .setRange(0, 100, percentage)
                .build());
        textPercentage.setText(percentage + " %");
    }

    private int getColorPercentage(float percentage) {
        if (0 <= percentage && percentage <= 14.99f) {
            return Color.GREEN;
        }

        if (15f <= percentage && percentage <= 25f) {
            return Color.YELLOW;
        }

        if (25.1f <= percentage && percentage <= 33f) {
            return Color.argb(255, 255, 153, 0);
        }

        if (33f <= percentage) {
            return Color.RED;
        }

        return Color.argb(255, 109, 23, 49);
    }

    private void getCaso() {
        if (consultaResponseList == null) {
            DecrimRequestSearchByIdCaso searchByIdCaso = new DecrimRequestSearchByIdCaso();
            searchByIdCaso.setIdCaso(Integer.parseInt(idCaso));
            searchByIdCaso.setEnvirontment(1);
            searchByIdCaso.setUserName(PerfilPersona.getUsernameDecrimService());
            searchByIdCaso.setPassword(PerfilPersona.getPasswordDecrimService());
            PerfilPersona.searchByIdCaso(searchByIdCaso, new Callback<DecrimResponseEnvelopeSearchById>() {
                @Override
                public void onResponse(Call<DecrimResponseEnvelopeSearchById> call, Response<DecrimResponseEnvelopeSearchById> response) {

                    if (response != null) {
                        consultaResponseList = response.body().getBody().getSearchByIdCasoResponse().getSearchByidCasoResult();
                        ConsultaResponse lastConsultaResponse = consultaResponseList.get(consultaResponseList.size() - 1);
                        busquedaListaNegra(lastConsultaResponse);

                    } else {
                        Toast.makeText(getBaseContext(), "Hubo un error", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<DecrimResponseEnvelopeSearchById> call, Throwable t) {
                    LoadingDialog.closeDialog();
                    Toast.makeText(getBaseContext(), "Hubo un error grave:" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            renderData(null);
        }
    }

    private void busquedaListaNegra(final ConsultaResponse lastConsultaResponse) {
        ListaNegraData listaNegraData = new ListaNegraData();
        listaNegraData.setNombreCompleto(lastConsultaResponse.getpNombre() + " " +
                lastConsultaResponse.getsNombre() +  " " +
                lastConsultaResponse.getpApellido() + " " +
                lastConsultaResponse.getsApellido());
        listaNegraData.setNumDoc(lastConsultaResponse.getNumeroIdentificacion());
        ListaNegraData.findResults(listaNegraData, new Callback<ResponseManagerListaNegra>() {
            @Override
            public void onResponse(
                Call<ResponseManagerListaNegra> call, Response<ResponseManagerListaNegra> response) {
                if (response != null && response.body() != null) {
                    listaNegraDataList = response.body().getObject();
                    renderData(lastConsultaResponse);
                }
            }

            @Override
            public void onFailure(Call<ResponseManagerListaNegra> call, Throwable t) {
                LoadingDialog.closeDialog();
            }
        });
    }

    private int getImageFromResultado(String resultado) {

        if (resultado == null || resultado.contains("NO PROCESADO") || resultado.contains("PENDIENTE")) {
            return R.drawable.lupa;
        }

        if (resultado.toLowerCase().contains("RECHAZAD".toLowerCase()) || resultado.toLowerCase().contains("Ilegible".toLowerCase()) || resultado.toLowerCase().contains("DEVUELTO".toLowerCase()) || resultado.toLowerCase().contains("ningun".toLowerCase())) {
            return R.drawable.denegaded;
        }

        return R.drawable.aprovaded;
    }

    private void renderData(ConsultaResponse lastConsultaResponse) {
        int score = 0;

        if (lastConsultaResponse != null) {
            lblNombreCompleto.setText(lastConsultaResponse.getpNombre() + " " +
                lastConsultaResponse.getsNombre() + " " +
                lastConsultaResponse.getpApellido() + " " +
                lastConsultaResponse.getsApellido());
            lblNumDocumento.setText(lastConsultaResponse.getNumeroIdentificacion());

            if (imgUsuarioData.contains("/upload")) {
                String urlImage =
                    Uri.parse(ServiceCommunication.BASE_URL + imgUsuarioData).toString()
                        .replace("//", "/");
                Picasso.with(this).load(urlImage).resize(200, 200).into(imgUsuario);
            } else {
                imgUsuario.setImageBitmap(PerfilPersona.getBitmapFromBase64(imgUsuarioData));
            }
        }
        if (consultaResponseList != null && !consultaResponseList.isEmpty()) {
            lastConsultaResponse = consultaResponseList.get(consultaResponseList.size() - 1);

            //General
            lblAnalisisGeneral.setText(lastConsultaResponse.getResultadoValidacion());
            int imgAG = getImageFromResultado(lastConsultaResponse.getResultadoValidacion());
            imgAnalisisGeneral.setImageResource(imgAG);
            if (imgAG == R.drawable.denegaded &&
                !TextUtils.isEmpty(lastConsultaResponse.getObservaciones())) {
                final ConsultaResponse finalLastConsultaResponse = lastConsultaResponse;
                containerAnalisisGeneral.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showDialogWithInfoForAnalisisGeneral(
                            finalLastConsultaResponse.getObservaciones());
                    }
                });
            }

            //score += (imgAG == R.drawable.aprovaded || imgAG == R.drawable.lupa) ? 20 : 0;

            //Documento
            String analisisDocumento = lastConsultaResponse.getResultadoIdentificacion() != null ?
                lastConsultaResponse.getResultadoIdentificacion() :
                "PENDIENTE";
            lblAnalisisDocumento.setText(analisisDocumento);
            int imgDoc = getImageFromResultado(analisisDocumento);
            imgAnalisisDocumento.setImageResource(imgDoc);

            //score += (imgDoc== R.drawable.aprovaded || imgDoc == R.drawable.lupa) ? 20 : 0;

            //Huella
            String analisisHuella = lastConsultaResponse.getResultadoHuella() != null ?
                lastConsultaResponse.getResultadoHuella() :
                "PENDIENTE";
            lblAnalisisHuella.setText(analisisHuella);
            int imgH = getImageFromResultado(analisisHuella);
            imgAnalisisHuella.setImageResource(imgH);

            //score += (imgH == R.drawable.aprovaded || imgH == R.drawable.lupa) ? 20 : 0;
        }

        //Base de datos
        StringBuilder resultadoBaseDeDatos = new StringBuilder();
        //Porcentaje Básico.
        score = 29;
        if (listaNegraDataList.size() > 0) {
            final ListaNegraData listaNegraData = listaNegraDataList.get(listaNegraDataList.size() - 1);
            lblAnalisisBasesDeDatos.setText("Información Negativa");
            imgAnalisisBasesDeDatos.setImageResource(R.drawable.denegaded);
            containerAnalisisBasesDeDatos.setClickable(true);
            containerAnalisisBasesDeDatos.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDialogWithInfo(listaNegraDataList);
                }
            });

            boolean hasReinsertados = false;
            int capturas = 0;
            int denuncias = 0;
            for (ListaNegraData data : listaNegraDataList) {

                resultadoBaseDeDatos.append(getListaNegraStringInfo(data).toString());
                ListaNegraData.TipoListaNegra tipoListaNegra = ListaNegraData.TipoListaNegra.getTipoListaNegra(data.getTipo());
                switch (tipoListaNegra) {
                    case CAPTURAS:
                            capturas++;
                        break;
                    case DENUNCIAS:
                            denuncias++;
                        break;
                    case REINSERTADOS:
                        if (!hasReinsertados) {
                            hasReinsertados = true;
                            score += 4;
                        }
                        break;
                }

            }
            if (capturas > 0) {
                if (capturas == 1) {
                    score += 4;
                }
                if (capturas >= 2) {
                    score += 8;
                }
            }

            if (denuncias> 0) {
                if (denuncias == 1) {
                    score += 1;
                }
                if (denuncias == 2) {
                    score += 2;
                }
                if (denuncias >= 3) {
                    score += 3;
                }
            }
        } else {
            lblAnalisisBasesDeDatos.setText("Aprobado");
            imgAnalisisBasesDeDatos.setImageResource(R.drawable.aprovaded);
            resultadoBaseDeDatos = new StringBuilder("Aprobado");
        }

        final int porcentajeRiesgo = score;
        setPercentData(score);

        if (lastConsultaResponse != null) {
            lastConsultaResponse.insertarValidacion(resultadoBaseDeDatos.toString(),
                new Callback<ResponseManager>() {
                    @Override
                    public void onResponse(Call<ResponseManager> call,
                        Response<ResponseManager> response) {

                    }

                    @Override
                    public void onFailure(Call<ResponseManager> call, Throwable t) {

                    }
                });
        }

        final ConsultaResponse finalLastConsultaResponse1 = lastConsultaResponse;
        btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                view.setVisibility(View.GONE);
                Toast.makeText(ResulradoValidacionActivity.this, "Descargando... \nEsto puede tomar alrededor de 30 segundos por favor espere.", Toast.LENGTH_LONG).show();
                String listaNegraInfo = "";
                if (listaNegraDataList.isEmpty()) {
                    listaNegraInfo = "Aprobado";
                } else {
                    StringBuilder listaNegraInfoSB = new StringBuilder();
                    for (ListaNegraData listaNegraData : listaNegraDataList) {
                        listaNegraInfoSB.append(getListaNegraStringInfoForHTML(listaNegraData));
                    }
                    listaNegraInfo = listaNegraInfoSB.toString();
                }
                PdfFileRequest fileRequest = new PdfFileRequest();
                fileRequest.setIdCaso(idCaso);
                if (finalLastConsultaResponse1 != null && usuario.getTipo() == 1) {
                    fileRequest.setResultadoValidacion(finalLastConsultaResponse1.getResultadoValidacion());
                    fileRequest.setResultadoIdentificacion(finalLastConsultaResponse1.getResultadoIdentificacion());
                    fileRequest.setResultadoHuella(finalLastConsultaResponse1.getResultadoHuella());
                } else {
                    fileRequest.setResultadoValidacion("tipo2");
                    fileRequest.setResultadoIdentificacion("tipo2");
                    fileRequest.setResultadoHuella("tipo2");
                }

                fileRequest.setListaNegra(listaNegraInfo.replace("\n", " ").replace("\r", " "));
                fileRequest.setPorcentajeRiesgo(String.valueOf(porcentajeRiesgo) + " %");

                PdfFileRequest.generatePDFFile(fileRequest, new Callback<ResponseManagerPdfGenerate>() {
                    @Override
                    public void onResponse(Call<ResponseManagerPdfGenerate> call, Response<ResponseManagerPdfGenerate> response) {
                        if (response != null && response.body() != null) {
                            view.setVisibility(View.VISIBLE);
                            String url = ServiceCommunication.BASE_URL + "decrimdata/getfile/";
                            String file = response.body().getObject();
                            Intent intentDownloadFile = new Intent(Intent.ACTION_VIEW);
                            intentDownloadFile.setData(Uri.parse(url + file));
                            startActivity(intentDownloadFile);
                        } else {
                            if (response != null && response.errorBody() != null) {
                                try {
                                    Toast.makeText(ResulradoValidacionActivity.this, "Error descargando:" + response.errorBody().string(), Toast.LENGTH_LONG).show();
                                } catch (IOException exp) {
                                    Toast.makeText(ResulradoValidacionActivity.this, "Error descargando", Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseManagerPdfGenerate> call, Throwable t) {
                        view.setVisibility(View.VISIBLE);
                        Toast.makeText(ResulradoValidacionActivity.this, "Error descargando:" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

        LoadingDialog.closeDialog();

    }

    private void showDialogWithInfo(List<ListaNegraData> listaNegraDataList) {
        // 1. Instantiate an AlertDialog.Builder with its constructor
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        // 2. Chain together various setter methods to set the dialog characteristics
        StringBuilder message = new StringBuilder();
        for (ListaNegraData listaNegraData : listaNegraDataList) {
            message.append(getListaNegraStringInfo(listaNegraData).toString());
        }
        builder.setMessage(message.toString())
               .setTitle("Antecedentes Encontrados")
               .setCancelable(true);

        // 3. Get the AlertDialog from create()
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showDialogWithInfoForAnalisisGeneral(String observaciones) {
        // 1. Instantiate an AlertDialog.Builder with its constructor
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        // 2. Chain together various setter methods to set the dialog characteristics

        builder.setMessage(observaciones)
            .setTitle("Observaciones")
            .setCancelable(true);
        // 3. Get the AlertDialog from create()
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @NonNull
    private StringBuilder getListaNegraStringInfo(ListaNegraData listaNegraData) {
        StringBuilder message = new StringBuilder();
        message.append(listaNegraData.getUbicacion());
        message.append("\n");
        message.append(listaNegraData.getDescrip1());
        message.append("\n");
        message.append(listaNegraData.getDescrip2());
        message.append("\n");
        message.append(listaNegraData.getFecha());
        message.append("\n");
        message.append("\n");
        return message;
    }

    @NonNull
    private StringBuilder getListaNegraStringInfoForHTML(ListaNegraData listaNegraData) {
        StringBuilder message = new StringBuilder();
        message.append(listaNegraData.getUbicacion());
        message.append("<br>");
        message.append(listaNegraData.getDescrip1());
        message.append("<br>");
        message.append(listaNegraData.getDescrip2());
        message.append("<br>");
        message.append(listaNegraData.getFecha());
        message.append("<br>");
        message.append("<br>");
        return message;
    }
}

package com.antifraudminds.mindanatifraud.activities;

import android.content.ClipData;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.antifraudminds.mindanatifraud.R;
import com.antifraudminds.mindanatifraud.model.ResponseManager;
import com.antifraudminds.mindanatifraud.model.Solicitud;
import com.google.gson.internal.LinkedTreeMap;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kikeval on 7/09/16.
 */
public class DenunciaActivity extends AppCompatActivity {

    public static final int ID_SERVICIO_DENUNCIA = 7;
    private static final int FILE_SELECT_CODE = 0x34;
    @BindView(R.id.txtDescripcion)
    EditText txtDescripcion;

    @BindView(R.id.btnEnviar)
    Button btnEnviar;
    @BindView(R.id.btnSelectFile)
    Button btnSelectFile;

    private int idEmpresa;
    private int idUsuario;
    private List<File> archivoAdjunto = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_denuncia);
        ButterKnife.bind(this);
        setListener();

        archivoAdjunto = new ArrayList<>();

        if (getIntent().hasExtra(LoginActivity.KEY_ID_EMPRESA)) {
            idEmpresa = getIntent().getIntExtra(LoginActivity.KEY_ID_EMPRESA, -1);
            idUsuario = getIntent().getIntExtra(LoginActivity.KEY_ID_USUARIO, -1);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FILE_SELECT_CODE:
                if (resultCode == RESULT_OK) {
                    // Get the Uri of the selected file
                    ClipData clipData = data.getClipData();
                    if (clipData != null) {
                        for (int index = 0; index < clipData.getItemCount(); index++) {
                            ClipData.Item item = clipData.getItemAt(index);
                            Uri uri = item.getUri();
                            String path = getPath(this, uri);
                            if (path != null) {
                                archivoAdjunto.add(new File(path));
                            }
                        }
                    } else {
                        Uri uri = data.getData();
                        String path = getPath(this, uri);
                        if (path != null) {
                            archivoAdjunto.add(new File(path));
                        }
                    }
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void setListener() {
        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtDescripcion.getText().toString().length() > 0) {
                    Solicitud solicitud = new Solicitud();
                    solicitud.setIdEmpresa(idEmpresa);
                    solicitud.setIdUsuario(idUsuario);
                    solicitud.setIdServicio(ID_SERVICIO_DENUNCIA);
                    solicitud.setTituloSolicitud("Denuncia Empresa: " + idEmpresa);
                    solicitud.setTxtRequerimiento(txtDescripcion.getText().toString());
                    if (archivoAdjunto != null && !archivoAdjunto.isEmpty()) {
                        Solicitud.sendSolicitud(solicitud, archivoAdjunto, new Callback<ResponseManager>() {
                            @Override
                            public void onResponse(Call<ResponseManager> call, Response<ResponseManager> response) {
                                if (response.body() != null) {
                                    LinkedTreeMap responseObject = (LinkedTreeMap) response.body().getObject();
                                    String codigo = responseObject.get("consecutivo").toString();
                                    showDialogWithInfo(codigo);
                                } else {
                                    Toast.makeText(getBaseContext(), "Ocurrió una falla.", Toast.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseManager> call, Throwable t) {
                                Toast.makeText(getBaseContext(), "Ocurrió una falla.", Toast.LENGTH_LONG).show();
                            }
                        });
                    } else {
                        Solicitud.sendSolicitud(solicitud, new Callback<ResponseManager>() {
                            @Override
                            public void onResponse(Call<ResponseManager> call, Response<ResponseManager> response) {
                                if (response.body() != null) {
                                    LinkedTreeMap responseObject = (LinkedTreeMap) response.body().getObject();
                                    String codigo = responseObject.get("consecutivo").toString();
                                    showDialogWithInfo(codigo);
                                } else {
                                    Toast.makeText(getBaseContext(), "Ocurrió una falla.", Toast.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseManager> call, Throwable t) {
                                Toast.makeText(getBaseContext(), "Ocurrió una falla.", Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                } else {
                    Toast.makeText(getBaseContext(), "Debe ingresar la descripción", Toast.LENGTH_LONG).show();
                }
            }
        });

        btnSelectFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("*/*");
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                //startActivityForResult(Intent.createChooser(intent, "Seleccione un archivo"), FILE_SELECT_CODE);
                startActivityForResult(intent, FILE_SELECT_CODE);
            }
        });
    }

    private void showDialogWithInfo(String codigo) {
        // 1. Instantiate an AlertDialog.Builder with its constructor
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        // 2. Chain together various setter methods to set the dialog characteristics
        builder.setMessage("No. " + codigo + "\nToma nota de número para realizar seguimiento al caso.")
                .setTitle("Código único de Denuncia")
                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        finish();
                    }
                })
                .setCancelable(true);

        // 3. Get the AlertDialog from create()
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * Get a file path from a Uri. This will get the the path for Storage Access
     * Framework Documents, as well as the _data field for the MediaStore and
     * other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @author paulburke
     */
    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
                // ExternalStorageProvider
                if (isExternalStorageDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];

                    if ("primary".equalsIgnoreCase(type)) {
                        return Environment.getExternalStorageDirectory() + "/" + split[1];
                    }

                    // TODO handle non-primary volumes
                }
                // DownloadsProvider
                else if (isDownloadsDocument(uri)) {

                    final String id = DocumentsContract.getDocumentId(uri);
                    final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                    return getDataColumn(context, contentUri, null, null);
                }
                // MediaProvider
                else if (isMediaDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];

                    Uri contentUri = null;
                    if ("image".equals(type)) {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                    } else if ("video".equals(type)) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                    } else if ("audio".equals(type)) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                    }

                    final String selection = "_id=?";
                    final String[] selectionArgs = new String[] {
                        split[1]
                    };

                    return getDataColumn(context, contentUri, selection, selectionArgs);
                }
            }
            // MediaStore (and general)
            else if ("content".equalsIgnoreCase(uri.getScheme())) {

                // Return the remote address
                if (isGooglePhotosUri(uri))
                    return uri.getLastPathSegment();

                return getDataColumn(context, uri, null, null);
            }
            // File
            else if ("file".equalsIgnoreCase(uri.getScheme())) {
                return uri.getPath();
            }
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @param selection (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
        String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
            column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }


}

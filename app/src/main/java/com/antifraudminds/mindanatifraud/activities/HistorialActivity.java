package com.antifraudminds.mindanatifraud.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.antifraudminds.mindanatifraud.R;
import com.antifraudminds.mindanatifraud.adapter.HistoryValidateAdapter;
import com.antifraudminds.mindanatifraud.model.DecrimData;
import com.antifraudminds.mindanatifraud.model.HistoryValidateObject;
import com.antifraudminds.mindanatifraud.model.ResponseManager;
import com.antifraudminds.mindanatifraud.model.ResponseManagerDecrimData;
import com.antifraudminds.mindanatifraud.model.Usuario;
import com.antifraudminds.mindanatifraud.views.LoadingDialog;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kikeval on 8/09/16.
 */
public class HistorialActivity  extends AppCompatActivity{
    public static final String KEY_ID_CASO = "KEY_ID_CASO";
    public static final String KEY_IMAGE = "KEY_IMAGE";
    private ListView listView;
    private HistoryValidateAdapter mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historial);
        listView = (ListView) findViewById(R.id.listData);
        setAdapterValues();
    }

    private void setAdapterValues() {
        Usuario usuario = null;
        try {
            usuario = Usuario.retrieveUsuarioLoginData(getSharedPreferences(Usuario.KEY_USUARIO, MODE_PRIVATE));
        } catch (IOException e) {
            e.printStackTrace();
        }
        LoadingDialog.showDialog(this);
        LoadingDialog.updateText("Obteniendo Casos");
        DecrimData.getAll(String.valueOf(usuario.getId()), new Callback<ResponseManagerDecrimData>() {
            @Override
            public void onResponse(Call<ResponseManagerDecrimData> call, Response<ResponseManagerDecrimData> response) {
                LoadingDialog.closeDialog();
             if (response.body() != null) {
                 List<HistoryValidateObject> subjects = getList(response.body().getObject());
                 mAdapter = new HistoryValidateAdapter(getApplication(), R.layout.rowlayout, subjects);
                 listView.setAdapter(mAdapter);
                 listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                     @Override
                     public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                         HistoryValidateObject historyValidateObject = mAdapter.getItem(position);

                         Toast.makeText(HistorialActivity.this, historyValidateObject.nameValidate + " Clicked", Toast.LENGTH_SHORT).show();
                         Intent nextActivity = new Intent(HistorialActivity.this, ResulradoValidacionActivity.class);
                         nextActivity.putExtra(KEY_ID_CASO, historyValidateObject.idCaso);
                         nextActivity.putExtra(KEY_IMAGE, historyValidateObject.imgStatus);
                         startActivity(nextActivity);
                     }
                 });

             }
            }

            @Override
            public void onFailure(Call<ResponseManagerDecrimData> call, Throwable t) {
                LoadingDialog.closeDialog();
                Toast.makeText(getBaseContext(), "Error:" + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();

            }
        });
    }

    private List<HistoryValidateObject> getList(List<DecrimData> decrimDataList) {
        List<HistoryValidateObject> validateObjectList = new ArrayList<>();
        for (DecrimData decrimData : decrimDataList) {
            HistoryValidateObject historyValidateObject = new HistoryValidateObject();
            historyValidateObject.idCaso = String.valueOf(decrimData.getIdCaso());
            historyValidateObject.nameValidate = decrimData.getNombres() + " " + decrimData.getApellidos();
            historyValidateObject.identification = decrimData.getNumDocumento();
            historyValidateObject.imgStatus = decrimData.getFoto();
            validateObjectList.add(historyValidateObject);
        }
        return validateObjectList;
    }
}

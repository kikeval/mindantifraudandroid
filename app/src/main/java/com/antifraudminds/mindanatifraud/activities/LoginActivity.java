package com.antifraudminds.mindanatifraud.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.antifraudminds.mindanatifraud.MainActivity;
import com.antifraudminds.mindanatifraud.R;
import com.antifraudminds.mindanatifraud.fragment.ValidarMenuFragment;
import com.antifraudminds.mindanatifraud.model.ResponseManager;
import com.antifraudminds.mindanatifraud.model.Usuario;
import com.antifraudminds.mindanatifraud.views.LoadingDialog;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.internal.LinkedTreeMap;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Optional;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kikeval on 7/09/16.
 */
public class LoginActivity extends AppCompatActivity {

    public static final int TIPO = 1;
    public static final String KEY_NEXT_SCREEN = "keyNextScreen";
    public static final String KEY_LOGIN_TYPE = "keyLoginType";
    public static final String KEY_ID_EMPRESA = "ID_EMPRESA";
    public static final String KEY_ID_USUARIO = "ID_USUARIO";

    private LOGIN_TYPE loginType;
    private int idUsuario;

    public enum NEXT_SCREENS {
        VALIDAR,
        DENUNCIA;
    }

    public enum LOGIN_TYPE {
        NORMAL,
        QRCODE;
    }

    @Nullable
    @BindView(R.id.txtEmail)
    EditText txtEmail;
    @Nullable
    @BindView(R.id.txtPassword)
    EditText txtPassword;
    @Nullable
    @BindView(R.id.btnIngresar)
    Button btnIngresar;

    @Nullable
    @BindView(R.id.btnScan)
    Button btnScan;

    private int nextActivity;

    //region Activity life cycle
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(KEY_NEXT_SCREEN)) {
                nextActivity = bundle.getInt(KEY_NEXT_SCREEN);
            }

            if (bundle.containsKey(KEY_LOGIN_TYPE)) {
                loginType = bundle.getInt(KEY_LOGIN_TYPE) == LOGIN_TYPE.NORMAL.ordinal() ? LOGIN_TYPE.NORMAL : LOGIN_TYPE.QRCODE;
            }
        }

        setContentView(loginType == LOGIN_TYPE.NORMAL ? R.layout.login : R.layout.login_qrcode);
        ButterKnife.bind(this);
        setListeners();
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            checkPreviousLogin();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode & 0xFFFF) == IntentIntegrator.REQUEST_CODE && data != null && resultCode == Activity.RESULT_OK) {
            IntentResult scanResult = IntentIntegrator.parseActivityResult((requestCode & 0xFFFF), resultCode, data);
            String content = scanResult.getContents();
            final Usuario usuario = new Usuario(content.substring(0, content.indexOf("|")), content.substring(content.indexOf("|") + 1), TIPO);
            ingresar(usuario, false);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    //endregion

    private void setListeners() {
        if (btnIngresar != null) {
            btnIngresar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Usuario usuario =  new Usuario(txtEmail.getText().toString(), txtPassword.getText().toString(), TIPO);
                    ingresar(usuario, true);
                }
            });
        }
        if (btnScan != null) {
            btnScan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    scanQRCode();
                }
            });
        }
    }

    private void checkPreviousLogin() throws IOException {
        if (txtEmail != null && txtPassword != null) {
            Usuario usuario = Usuario.retrieveUsuarioLoginData(getSharedPreferences(Usuario.KEY_USUARIO, MODE_PRIVATE));
            if (usuario != null) {
                if (usuario.getEmail() != null) {
                    txtEmail.setText(usuario.getEmail());
                }
                if (usuario.getPassword() != null) {
                    txtPassword.setText(usuario.getPassword());
                }
                //openNextActivity(MainActivity.class);
            }
        }
    }

    private void ingresar(final Usuario usuario, final boolean saveLoginData) {
        LoadingDialog.showDialog(this);
        Usuario.authUser(usuario, new Callback<ResponseManager>() {
            @Override
            public void onResponse(Call<ResponseManager> call, Response<ResponseManager> response) {
                LoadingDialog.closeDialog();
                ResponseManager responseManager = response.body();
                if (responseManager != null) {
                    if (isUserAuthCorrectly(responseManager)) {
                        LinkedTreeMap responseObject = (LinkedTreeMap) responseManager.getObject();
                        idUsuario = ((Number) responseObject.get("idUsuario")).intValue();
                        if (saveLoginData) {
                            Usuario usuarioToSave = new Usuario(((Number) responseObject.get("id")).intValue(), responseObject.get("nombre")
                                                                                                                              .toString(),
                                    responseObject.get("email").toString(), usuario.getPassword(), responseObject.get("nit")
                                                                                                                 .toString(), responseObject.get
                                    ("ciudad").toString(), responseObject.get("logo")
                                                                         .toString(), ((Number) responseObject.get("idUsuario")).intValue(), (
                                    (Number) responseObject.get("tipo")).intValue());

                            try {
                                Usuario.saveUserLoginData(usuarioToSave, getSharedPreferences(Usuario.KEY_USUARIO, MODE_PRIVATE).edit());
                            } catch (JsonProcessingException e) {
                                e.printStackTrace();
                            }
                        }
                        openNextActivity(((Number) responseObject.get("id")).intValue(), nextActivity == NEXT_SCREENS.VALIDAR.ordinal() ?
                                                                                        ValidarActivity.class : DenunciaActivity.class);
                    } else {
                        Toast.makeText(getBaseContext(), "Hubo un error en el ingreso, intente de nuevo", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getBaseContext(), "Hubo un error en el ingreso, intente de nuevo", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseManager> call, Throwable t) {
                LoadingDialog.closeDialog();
                Toast.makeText(getBaseContext(), "Hubo un error en el ingreso, intente de nuevo", Toast.LENGTH_LONG).show();
            }
        });
    }

    public boolean isUserAuthCorrectly(ResponseManager responseManager) {
        return responseManager.getError() instanceof String && responseManager.getError().equals(ResponseManager.NO_ERROR) && responseManager.getObject() != null;
    }

    private void scanQRCode() {
        (new IntentIntegrator(this)).setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES).initiateScan();
    }

    private void openNextActivity(int idEmpresa, Class<? extends Activity> activityClass) {
        Intent intent = new Intent(LoginActivity.this, activityClass);
        intent.putExtra(KEY_ID_EMPRESA, idEmpresa);
        if (activityClass.equals(DenunciaActivity.class)) {
            intent.putExtra(KEY_ID_USUARIO, idUsuario);
        }

        startActivity(intent);
        finish();
    }

    public static boolean isLogued(Context context) throws IOException {
        Usuario usuario = Usuario.retrieveUsuarioLoginData(context.getSharedPreferences(Usuario.KEY_USUARIO, MODE_PRIVATE));
        return  usuario != null && usuario.getEmail() != null;
    }
}
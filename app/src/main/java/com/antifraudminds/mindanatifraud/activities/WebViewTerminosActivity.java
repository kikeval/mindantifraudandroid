package com.antifraudminds.mindanatifraud.activities;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import com.antifraudminds.mindanatifraud.R;
import com.antifraudminds.mindanatifraud.model.ServiceCommunication;
import com.antifraudminds.mindanatifraud.model.UsuarioServicio;

/**
 * Created by kikeval on 8/09/16.
 */
public class WebViewTerminosActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view_terminos);
        String [] array = new String[6];

        WebView myWebView = (WebView) this.findViewById(R.id.webView);
        myWebView.loadUrl(ServiceCommunication.BASE_URL + "termino.html");

    }

}

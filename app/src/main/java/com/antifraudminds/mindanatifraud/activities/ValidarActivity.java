package com.antifraudminds.mindanatifraud.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.widget.Toast;

import com.antifraudminds.mindanatifraud.MainActivity;
import com.antifraudminds.mindanatifraud.R;
import com.antifraudminds.mindanatifraud.baseactivity.AbstractFingerPrintReaderActivity;
import com.antifraudminds.mindanatifraud.fragment.FormularioFragment;
import com.antifraudminds.mindanatifraud.fragment.ValidarMenuFragment;
import com.antifraudminds.mindanatifraud.model.HuellaResponseData;
import com.microblink.activity.Pdf417ScanActivity;
import com.microblink.recognizers.BaseRecognitionResult;
import com.microblink.recognizers.RecognitionResults;
import com.microblink.recognizers.blinkbarcode.pdf417.Pdf417ScanResult;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

/**
 * Created by kikeval on 7/09/16.
 */

public class ValidarActivity extends AbstractFingerPrintReaderActivity implements FormularioFragment.FormularioFragmentActions, ValidarMenuFragment.ValidarMenuFragmentActions {

    private static final int TOMAR_FOTO_USUARIO = 5;
    private static final String CURRENT_FRAGMENT = "currentFragment";
    public static final String DATA = "data";
    public static final String FOTO_USER = "fotoUser";

    Fragment currentFragment;
    private String uriFotoUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validar);

        if (savedInstanceState == null) {
            addFragment(MainActivity.FragmentNavigationManager.VALIDAR_MENU_FRAGMENT);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (getSupportFragmentManager().getFragments().contains(currentFragment)) {
            getSupportFragmentManager().putFragment(outState, CURRENT_FRAGMENT, currentFragment);
            outState.putString(FOTO_USER, uriFotoUser);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState != null) {
            //Restore the fragment's instance
            currentFragment = getSupportFragmentManager().getFragment(savedInstanceState, CURRENT_FRAGMENT);
            if (savedInstanceState.containsKey(FOTO_USER)) {
                uriFotoUser = savedInstanceState.getString(FOTO_USER);
            }
        }
    }

// show data to camera
// show animate items biometric Image user

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {
            if (requestCode == CamDocumentActivity.GET_FOTOS_CEDULA) {
                FormularioFragment formularioFragment = (FormularioFragment) getCurrentFragment(FormularioFragment.class);
                formularioFragment.setImageDocumentoValidar(data.getStringExtra(CamDocumentActivity.IMAGE_TIRO_URI), data.getStringExtra(CamDocumentActivity.IMAGE_RETIRO_URI));
                return;
            }
            if (requestCode == TOMAR_FOTO_USUARIO) {
                FormularioFragment formularioFragment = (FormularioFragment) getCurrentFragment(FormularioFragment.class);
                formularioFragment.setImageUsuario(uriFotoUser);
                return;
            }

            if (requestCode == FirmaUsuarioActivity.REQUEST_FIRMA_USUARIO) {
                FormularioFragment formularioFragment = (FormularioFragment) getCurrentFragment(FormularioFragment.class);
                String path = data.getStringExtra(FirmaUsuarioActivity.FIRMA_PAD_IMAGEN);
                formularioFragment.setImageFirma(path);
            }

            if (resultCode == Pdf417ScanActivity.RESULT_OK && data != null) {
                try {
                    // perform processing of the data here

                    // for example, obtain parcelable recognition result
                    RecognitionResults result = data.getParcelableExtra(Pdf417ScanActivity.EXTRAS_RECOGNITION_RESULTS);
                    if (result != null) {

                        // get array of recognition results
                        BaseRecognitionResult[] resultArray = result.getRecognitionResults();
                        // Each element in resultArray inherits BaseRecognitionResult class and
                        // represents the scan result of one of activated recognizers that have
                        // been set up. More information about this can be found in
                        // "Recognition settings and results" chapter
                        if (resultArray.length > 0 && resultArray[0] instanceof Pdf417ScanResult) {
                            String content = ((Pdf417ScanResult) resultArray[0]).getStringData();
                            FormularioFragment formularioFragment = (FormularioFragment) getCurrentFragment(FormularioFragment.class);
                            formularioFragment.parseResult(content);
                        }
                    }
                } catch (Exception exp) {
                    Toast.makeText(this, "Su cédula no pudo ser leida, digite sus datos manualmente.", Toast.LENGTH_LONG).show();
                    return;
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (getSupportFragmentManager().getBackStackEntryCount() <= 0) {
            finish();
        }
    }

    private Fragment getCurrentFragment(Class <? extends Fragment> fragmentClass) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        Fragment fragment = fragments.get(fragments.size()-1);
        fragment = fragment != null ? fragment : fragmentManager.findFragmentByTag(MainActivity.FragmentNavigationManager.FORMULARIO_FRAGMENT.getTagName());
        return fragmentClass.isInstance(fragment) ? fragment : null;
    }

    private void addFragment(MainActivity.FragmentNavigationManager fragmentNavigationManager) {
        try {
            Fragment fragment = fragmentNavigationManager.getFragmentClass().newInstance();
            currentFragment = fragment;
            performFragmentTransaction(fragmentNavigationManager, fragment, false);
        } catch (Exception exp) {

        }
    }

    private void replaceFragment(MainActivity.FragmentNavigationManager fragmentNavigationManager) {
        try {
            Fragment fragment = fragmentNavigationManager.getFragmentClass().newInstance();
            currentFragment = fragment;
            performFragmentTransaction(fragmentNavigationManager, fragment, true);
        } catch (Exception exp) {

        }
    }

    private void performFragmentTransaction(MainActivity.FragmentNavigationManager fragmentNavigationManager, Fragment fragment, boolean isReplace) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        if (!isReplace) {
            transaction.add(R.id.validar_content, fragment, fragmentNavigationManager.getTagName());
        } else {
            transaction.replace(R.id.validar_content, fragment, fragmentNavigationManager.getTagName());
        }
        transaction.addToBackStack(fragmentNavigationManager.getTagName());
        transaction.commit();
    }

    //region FormularioFragment Actions
    @Override
    public void verTerminos() {
        Intent intentTerminos = new Intent(this, WebViewTerminosActivity.class);
        startActivity(intentTerminos);
    }

    @Override
    public void tomarFoto() {
        Intent intentFotoUsuario = new Intent();
        intentFotoUsuario.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
        uriFotoUser = CamDocumentActivity.createFileName("userphoto_");
        Uri fotoUserUri = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            fotoUserUri = CamDocumentActivity.getFileUriProvider(this, uriFotoUser);
            intentFotoUsuario.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        } else {
            fotoUserUri = CamDocumentActivity.getFileUri(uriFotoUser);
        }
        intentFotoUsuario.putExtra(MediaStore.EXTRA_OUTPUT, fotoUserUri);
        startActivityForResult(intentFotoUsuario, TOMAR_FOTO_USUARIO);
    }
    @Override
    public void tomarFotoCedula() {
        Intent intentDocumento = new Intent(this, CamDocumentActivity.class);
        startActivityForResult(intentDocumento, CamDocumentActivity.GET_FOTOS_CEDULA);
    }

    @Override
    public void pedirFirma() {
        Intent intentPedirFirma = new Intent(this, FirmaUsuarioActivity.class);
        startActivityForResult(intentPedirFirma, FirmaUsuarioActivity.REQUEST_FIRMA_USUARIO);
    }
    //endregion

    @Override
    public void stateChanged(int state) {
        FormularioFragment formularioFragment = currentFragment != null ? (FormularioFragment) currentFragment : (FormularioFragment) getCurrentFragment
                (FormularioFragment.class);
        if (formularioFragment != null) {
            formularioFragment.stateChanged(state);
        }
    }

    @Override
    public void imageCaptured(HuellaResponseData data) {
        FormularioFragment formularioFragment = currentFragment != null ? (FormularioFragment) currentFragment : (FormularioFragment) getCurrentFragment
                (FormularioFragment.class);
        if (formularioFragment != null) {
            formularioFragment.imageCaptured(data);
        }
    }

    public void connect() {
        setRequestConnectDevice();
    }

    public void capture() {
        sendCommand();
    }

    @Override
    public void verHistorial() {
        Intent historialActivity = new Intent(this, HistorialActivity.class);
        startActivity(historialActivity);
    }

    @Override
    public void validar() {
        replaceFragment(MainActivity.FragmentNavigationManager.FORMULARIO_FRAGMENT);
    }

    public static Bitmap getBitmapFromFile(String file) throws FileNotFoundException {
        File imgFile = new File(CamDocumentActivity.getFileUri(file).getPath());
        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
        return myBitmap;
    }

    public static Bitmap getScaledTo(String file, int size) throws FileNotFoundException {
        Bitmap theBitmap = getBitmapFromFile(file);
        boolean isHeightest = theBitmap.getHeight() > theBitmap.getWidth();
        float proportion = isHeightest ? (((float)size) / theBitmap.getHeight()) : (((float)size) / theBitmap.getWidth());
        return Bitmap.createScaledBitmap(theBitmap,(int) (theBitmap.getWidth() * proportion), (int)(theBitmap.getHeight() * proportion), true);
    }

    public static Bitmap getScaledTo(String file, int width, int height) throws FileNotFoundException {
        Bitmap theBitmap = getBitmapFromFile(file);
        return Bitmap.createScaledBitmap(theBitmap, width, height, true);
    }
 }

package com.antifraudminds.mindanatifraud.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;

import com.antifraudminds.mindanatifraud.R;
import com.simplify.ink.InkView;

import java.io.File;
import java.io.FileOutputStream;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FirmaUsuarioActivity extends Activity {


    public static int REQUEST_FIRMA_USUARIO = 85;
    public static String FIRMA_PAD_IMAGEN = "FIRMAPADIMAGEN";

    @BindView(R.id.signature_pad)
    InkView firmaPad;
    @BindView(R.id.btnConfirmar)
    Button btnConfirmar;
    @BindView(R.id.btnLimpiar)
    Button btnLimpiar;

    private Bitmap firmaImagen;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setResult(RESULT_CANCELED);
        setContentView(R.layout.activity_firma_usuario);
        ButterKnife.bind(this);

        btnConfirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firmaImagen = firmaPad.getBitmap(Color.argb(125, 255, 255, 255));
                setResultContent(saveToFile(firmaImagen));
                finish();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firmaPad.clear();
            }
        });
    }

    private String saveToFile(Bitmap bitmap) {
        String fileName = Environment.getExternalStorageDirectory() + "/firmaArchivo" + Math.round(Math.random() * 1000) + ".png";
        try {
            File file = new File(fileName);
            FileOutputStream fOut = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 85, fOut);
            fOut.flush();
            fOut.close();
        } catch (Exception exp) {

        }
        return fileName;
    }

    private void setResultContent(String fileName) {
        Intent intent = new Intent();
        intent.putExtra(FIRMA_PAD_IMAGEN, fileName);
        setResult(RESULT_OK, intent);
    }
}

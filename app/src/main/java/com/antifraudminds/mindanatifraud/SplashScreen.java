package com.antifraudminds.mindanatifraud;

import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.kishan.askpermission.AskPermission;
import com.kishan.askpermission.ErrorCallback;
import com.kishan.askpermission.PermissionCallback;
import com.kishan.askpermission.PermissionInterface;

public class SplashScreen extends AppCompatActivity {

    public static final int REQUEST_CODE = 4465;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            new AskPermission.Builder(this)
                    .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA,
                            Manifest.permission.BLUETOOTH_ADMIN)
                    .setCallback(new PermissionCallback() {
                        @Override
                        public void onPermissionsGranted(int requestCode) {
                            if (requestCode == REQUEST_CODE) {
                                initiateApp();
                            }
                        }

                        @Override
                        public void onPermissionsDenied(int requestCode) {
                            if (requestCode == REQUEST_CODE) {
                                Toast.makeText(SplashScreen.this, "La aplicación puede no funcionar como debería, debe aceptar los permisos requeridos por la aplicación", Toast.LENGTH_LONG).show();
                                finish();
                            }
                        }
                    })
                    .setErrorCallback(new ErrorCallback() {
                        @Override
                        public void onShowRationalDialog(PermissionInterface permissionInterface, int requestCode) {
                            Toast.makeText(SplashScreen.this, "La aplicación puede no funcionar como debería, debe aceptar los permisos requeridos por la aplicación", Toast.LENGTH_LONG).show();
                            permissionInterface.onDialogShown();
                            finish();
                        }

                        @Override
                        public void onShowSettings(PermissionInterface permissionInterface, int requestCode) {
                            Toast.makeText(SplashScreen.this, "La aplicación puede no funcionar como debería, debe aceptar los permisos requeridos por la aplicación", Toast.LENGTH_LONG).show();
                            permissionInterface.onSettingsShown();
                            finish();
                        }
                    })
                    .request(REQUEST_CODE);
        } else {
            initiateApp();
        }
    }

    private void initiateApp() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashScreen.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }, 4000);
    }

}

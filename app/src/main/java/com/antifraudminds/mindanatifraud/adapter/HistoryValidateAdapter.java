package com.antifraudminds.mindanatifraud.adapter;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.antifraudminds.mindanatifraud.R;
import com.antifraudminds.mindanatifraud.fragment.HistorialConsultasFragment;
import com.antifraudminds.mindanatifraud.model.HistoryValidateObject;
import com.antifraudminds.mindanatifraud.model.ServiceCommunication;
import com.antifraudminds.mindanatifraud.model.UsuarioServicio;
import com.antifraudminds.mindanatifraud.views.HistoryValidateHolder;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kikeval on 3/09/16.
 */
public class HistoryValidateAdapter extends ArrayAdapter<HistoryValidateObject> {

    private Context context;
    private int resource;
    private List<HistoryValidateObject> subjects = null;

    public HistoryValidateAdapter(Context context, int resource, List<HistoryValidateObject> subjects) {
        super(context, resource, subjects);
        this.context = context;
        this.resource = resource;
        this.subjects = subjects;
    }

    @Nullable
    @Override
    public HistoryValidateObject getItem(int position) {
        return subjects.get(position);
    }

    @Override
    public int getCount() {
        return subjects.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final HistoryValidateObject subject = subjects.get(position);

        HistoryValidateHolder validateHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.rowlayout, parent, false);
            //Instantiation of views
            validateHolder = new HistoryValidateHolder();
            validateHolder.textViewName = (TextView) convertView.findViewById(R.id.txtitem);
            validateHolder.textViewCC = (TextView) convertView.findViewById(R.id.txtitem2);
            validateHolder.imageViewStatus = (CircularImageView) convertView.findViewById(R.id.imgStatusValidate);
            convertView.setTag(validateHolder);
        } else {
            validateHolder = (HistoryValidateHolder) convertView.getTag();
        }

        //Updating views
        validateHolder.textViewName.setText(subject.nameValidate);
        validateHolder.textViewCC.setText(subject.identification);
        Picasso.with(context).load((ServiceCommunication.BASE_URL + subject.imgStatus).replace("//", "/")).resize(200,200).into(validateHolder.imageViewStatus);
        return convertView;
    }
}

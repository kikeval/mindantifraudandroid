package com.antifraudminds.mindanatifraud.model;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by gustavo.bonilla on 12/4/2016.
 */

public class DecrimData {
    private int id;
    private int idUsuario;
    private int idEmpresa;
    private int idCaso;
    private String nombres;
    private String apellidos;
    private String numDocumento;
    private String fechaNacimiento;
    private String sexo;
    private String rh;
    private String foto;

    public DecrimData() {
        id = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(int idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public int getIdCaso() {
        return idCaso;
    }

    public void setIdCaso(int idCaso) {
        this.idCaso = idCaso;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getNumDocumento() {
        return numDocumento;
    }

    public void setNumDocumento(String numDocumento) {
        this.numDocumento = numDocumento;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getRh() {
        return rh;
    }

    public void setRh(String rh) {
        this.rh = rh;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String huella) {
        this.foto = huella;
    }

    public static void sendDecrimData(DecrimData decrimData, Callback<ResponseManager> callback) {
        Retrofit retrofit = ServiceCommunication.getRetrofitBuilder();
        DecrimServicio decrimServicio = retrofit.create(DecrimServicio.class);
        Call<ResponseManager> auth = decrimServicio.insertar(decrimData);
        auth.enqueue(callback);
    }

    public static void getAll(String idEmpresa, Callback<ResponseManagerDecrimData> callback) {
        Retrofit retrofit = ServiceCommunication.getRetrofitBuilder();
        DecrimServicio decrimServicio = retrofit.create(DecrimServicio.class);
        Call<ResponseManagerDecrimData> getAll = decrimServicio.getAll(idEmpresa);
        getAll.enqueue(callback);
    }

    public static void getByIdCaso(String idCaso, Callback<ResponseManagerDecrimData> callback) {
        Retrofit retrofit = ServiceCommunication.getRetrofitBuilder();
        DecrimServicio decrimServicio = retrofit.create(DecrimServicio.class);
        Call<ResponseManagerDecrimData> getAll = decrimServicio.getByIdCaso(idCaso);
        getAll.enqueue(callback);
    }
}

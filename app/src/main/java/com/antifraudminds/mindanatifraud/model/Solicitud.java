package com.antifraudminds.mindanatifraud.model;

import android.net.Uri;
import android.support.annotation.NonNull;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by gustavo.bonilla on 11/30/2016.
 */

public class Solicitud {
    private int idEmpresa;
    private int idUsuario;
    private int idServicio;
    private String tituloSolicitud;
    private String txtRequerimiento;

    @NonNull
    private static MultipartBody.Part prepareFilePart(String partName, File file) {
        // https://github.com/iPaulPro/aFileChooser/blob/master/aFileChooser/src/com/ipaulpro/afilechooser/utils/FileUtils.java
        // use the FileUtils to get the actual file by uri

        // create RequestBody instance from file
        RequestBody requestFile =
            RequestBody.create(
                MediaType.parse("multipart/form-data"),
                file
            );

        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }

    public static void sendSolicitud(Solicitud solicitud, List<File> files, Callback<ResponseManager> callback) {
        Retrofit retrofit = ServiceCommunication.getRetrofitBuilder();

        List<MultipartBody.Part> filesMultiParts = new ArrayList<>();
        for (int index = 0; index < files.size(); index++) {
            filesMultiParts.add(prepareFilePart("file"+index, files.get(index)));
        }

        //Implementacion Vieja.
        //RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        //MultipartBody.Part body = MultipartBody.Part.createFormData("file0", file.getName(), requestFile);

        RequestBody idEmpresa = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(solicitud.getIdEmpresa()));
        RequestBody idUsuario = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(solicitud.getIdUsuario()));
        RequestBody idServicio = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(solicitud.getIdServicio()));
        RequestBody tituloSolicitud = RequestBody.create(MediaType.parse("multipart/form-data"), solicitud.getTituloSolicitud());
        RequestBody textRequerimiento = RequestBody.create(MediaType.parse("multipart/form-data"), solicitud.getTxtRequerimiento());

        SolicitudServicio servicio = retrofit.create(SolicitudServicio.class);
        Call<ResponseManager> create = servicio.solicitud(idEmpresa, idServicio, tituloSolicitud, textRequerimiento, idUsuario, filesMultiParts);
        create.enqueue(callback);
    }

    public static void sendSolicitud(Solicitud solicitud, Callback<ResponseManager> callback) {
        Retrofit retrofit = ServiceCommunication.getRetrofitBuilder();

        SolicitudServicio solicitudServicio = retrofit.create(SolicitudServicio.class);
        Call<ResponseManager> solicitudRequest = solicitudServicio.solicitudNoFile(solicitud);
        solicitudRequest.enqueue(callback);
    }


    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(int idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public int getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(int idServicio) {
        this.idServicio = idServicio;
    }

    public String getTituloSolicitud() {
        return tituloSolicitud;
    }

    public void setTituloSolicitud(String tituloSolicitud) {
        this.tituloSolicitud = tituloSolicitud;
    }

    public String getTxtRequerimiento() {
        return txtRequerimiento;
    }

    public void setTxtRequerimiento(String txtRequerimiento) {
        this.txtRequerimiento = txtRequerimiento;
    }
}

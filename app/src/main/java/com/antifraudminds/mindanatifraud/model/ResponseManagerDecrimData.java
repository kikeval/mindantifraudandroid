package com.antifraudminds.mindanatifraud.model;

import java.util.List;

/**
 * Created by gustavo.bonilla on 10/16/2016.
 */

public class ResponseManagerDecrimData {
    public static final String NO_ERROR = "NO_ERROR";
    private Object manager;
    protected List<DecrimData> object;
    private String mensaje;
    private Object error;

    public Object getManager() {
        return manager;
    }

    public List<DecrimData> getObject() {
        return object;
    }

    public String getMensaje() {
        return mensaje;
    }

    public Object getError() {
        return error;
    }
}

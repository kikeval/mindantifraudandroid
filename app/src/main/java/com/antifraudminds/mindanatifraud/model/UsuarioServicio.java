package com.antifraudminds.mindanatifraud.model;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by gustavo.bonilla on 10/16/2016.
 */

public interface UsuarioServicio {

    String EMAIL = "email";
    String PASSWORD = "password";
    String TIPO = "tipo";
    String ALL_DATA = "AllData";

    @Headers({
            "Content-Type: application/json",
            "User-Agent: AndroidAntiMindApp"
    })
    @POST("/usuario/auth")
    Call<ResponseManager> authUser(@Body Usuario usuario);
}

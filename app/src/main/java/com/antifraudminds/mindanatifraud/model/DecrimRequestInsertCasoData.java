package com.antifraudminds.mindanatifraud.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

/**
 * Created by gustavo.bonilla on 10/16/2016.
 */
@Root(name = "insertCaso", strict = false)
@Namespace(reference = "http://tempuri.org/")
public class DecrimRequestInsertCasoData {
    @Element(name = "environment", required = false)
    private int environtment;
    @Element(name = "userName", required = false)
    private String userName;
    @Element(name = "password", required = false)
    private String password;
    @Element(name = "idProducto", required = false)
    private int idProducto;
    @Element(name = "observaciones", required = false)
    private String observaciones;
    @Element(name = "idTipIde", required = false)
    private int idTipIde;
    @Element(name = "NIdentificacion", required = false)
    private String NIdentificacion;
    @Element(name = "pApellido", required = false)
    private String pApellido;
    @Element(name = "sApellido", required = false)
    private String sApellido;
    @Element(name = "pNombre", required = false)
    private String pNombre;
    @Element(name = "sNombre", required = false)
    private String sNombre;
    @Element(name = "fecNac", required = false)
    private String fecNac;
    @Element(name = "sexo", required = false)
    private String sexo;
    @Element(name = "rh", required = false)
    private String rh;
    @Element(name = "tren2D", required = false)
    private String tren2D;

    public int getEnvirontment() {
        return environtment;
    }

    public void setEnvirontment(int environtment) {
        this.environtment = environtment;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public int getIdTipIde() {
        return idTipIde;
    }

    public void setIdTipIde(int idTipIde) {
        this.idTipIde = idTipIde;
    }

    public String getNIdentificacion() {
        return NIdentificacion;
    }

    public void setNIdentificacion(String NIdentificacion) {
        this.NIdentificacion = NIdentificacion;
    }

    public String getpApellido() {
        return pApellido;
    }

    public void setpApellido(String pApellido) {
        this.pApellido = pApellido;
    }

    public String getsApellido() {
        return sApellido;
    }

    public void setsApellido(String sApellido) {
        this.sApellido = sApellido;
    }

    public String getpNombre() {
        return pNombre;
    }

    public void setpNombre(String pNombre) {
        this.pNombre = pNombre;
    }

    public String getsNombre() {
        return sNombre;
    }

    public void setsNombre(String sNombre) {
        this.sNombre = sNombre;
    }

    public String getFecNac() {
        return fecNac;
    }

    public void setFecNac(String fecNac) {
        this.fecNac = fecNac;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getRh() {
        return rh;
    }

    public void setRh(String rh) {
        this.rh = rh;
    }

    public String getTren2D() {
        return tren2D;
    }

    public void setTren2D(String tren2D) {
        this.tren2D = tren2D;
    }
}

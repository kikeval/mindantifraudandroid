package com.antifraudminds.mindanatifraud.model;

import java.io.Serializable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

/**
 * Created by gustavo.bonilla on 11/6/2016.
 */

public class Banner implements Serializable {
    private int id;
    private String urlImage;
    private String url;

    public int getId() {
        return id;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public String getUrl() {
        return url;
    }

    public static void getBanners(Callback<ResponseManagerBanner> callback) {
        Retrofit retrofit = ServiceCommunication.getRetrofitBuilder();
        BannerServicio servicio = retrofit.create(BannerServicio.class);
        Call<ResponseManagerBanner> banners = servicio.getBanner();
        banners.enqueue(callback);
    }
}

package com.antifraudminds.mindanatifraud.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.widget.Toast;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.convert.AnnotationStrategy;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.strategy.Strategy;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

/**
 * Representa a una persona a la cual se le va a
 *
 * @author gustavo.bonilla
 */
public class PerfilPersona {
    public static final int ID_PRODUCTO = 17;
    public static final String NUEVO_FORMATO_CEDULA = "PubDSK";
    private String numeroDocumento;
    private String nombres;
    private String apellidos;
    private String fechaNacimiento;
    private String RHSanguineo;
    private String sexo;
    private String tipoDocumento;
    private String observaciones;
    private HuellaResponseData huellaResponseData;
    private int idCaso;

    public static void enviarDatos(PerfilPersona perfilPersona, Callback<DecrimResponseEnvelope> callback) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();

        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        Strategy strategy = new AnnotationStrategy();

        Serializer serializer = new Persister(strategy);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .connectTimeout(2, TimeUnit.MINUTES)
                .writeTimeout(2, TimeUnit.MINUTES)
                .readTimeout(2, TimeUnit.MINUTES)
                .build();

        Retrofit retrofit =  new Retrofit.Builder()
                .addConverterFactory(SimpleXmlConverterFactory.create(serializer))
                .baseUrl("http://servicios.identidadenlinea.com")
                .client(okHttpClient)
                .build();

        DecrimRequestInsertCasoData data = new DecrimRequestInsertCasoData();
        data.setUserName(getUsernameDecrimService());
        data.setPassword(getPasswordDecrimService());
        data.setEnvirontment(1);
        data.setFecNac(perfilPersona.getFechaNacimiento());
        data.setIdProducto(ID_PRODUCTO);
        data.setNIdentificacion(perfilPersona.getNumeroDocumento());
        data.setIdTipIde(perfilPersona.getTipoDocumento());
        data.setObservaciones(perfilPersona.getObservaciones().isEmpty() ? "Sin observaciones" : perfilPersona.getObservaciones());
        data.setpApellido(perfilPersona.getApellidos().split(" ")[0]);
        data.setsApellido(perfilPersona.getApellidos().split(" ").length > 1 ? perfilPersona.getApellidos().split(" ")[1] : "");
        data.setpNombre(perfilPersona.getNombres().split(" ")[0]);
        data.setsNombre(perfilPersona.getNombres().split(" ").length > 1 ? perfilPersona.getNombres().split(" ")[1] : "");
        data.setRh(perfilPersona.getRHSanguineo());
        data.setSexo(perfilPersona.getSexo());
        //data.setTren2D(bytesToHex(perfilPersona.getHuellaResponseData().getData()));
        data.setTren2D(Arrays.toString(perfilPersona.getHuellaResponseData().getData()));

        DecrimRequestInsertCasoBody body = new DecrimRequestInsertCasoBody();
        body.setInsertCasoData(data);

        DecrimRequestInsertCasoEnvelope requestEnvelope = new DecrimRequestInsertCasoEnvelope();
        requestEnvelope.setBody(body);

        DecrimClientService clientService = retrofit.create(DecrimClientService.class);
        Call<DecrimResponseEnvelope> serviceResponse = clientService.insertCaso(requestEnvelope);
        serviceResponse.enqueue(callback);
    }

    public static void sendInsertImgCaso(final int idCaso, final List<Bitmap> imagenes, final Drawable.Callback callback) {
        int [] tipoImagenes = {1, 2, 4, 3};
        if (!imagenes.isEmpty()) {
            int tipoImagen = tipoImagenes[4 - imagenes.size()];
            Bitmap imageToInsert = imagenes.remove(0);
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();

            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            Strategy strategy = new AnnotationStrategy();

            Serializer serializer = new Persister(strategy);

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .addInterceptor(interceptor)
                    .connectTimeout(2, TimeUnit.MINUTES)
                    .writeTimeout(2, TimeUnit.MINUTES)
                    .readTimeout(2, TimeUnit.MINUTES)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .addConverterFactory(SimpleXmlConverterFactory.create(serializer))
                    .baseUrl("http://servicios.identidadenlinea.com")
                    .client(okHttpClient)
                    .build();

            DecrimRequestInsertImgCasoData insertImgCasoData = new DecrimRequestInsertImgCasoData();
            insertImgCasoData.setEnvirontment(1);
            insertImgCasoData.setIdCaso(idCaso);
            insertImgCasoData.setUserName(getUsernameDecrimService());
            insertImgCasoData.setPassword(getPasswordDecrimService());
            insertImgCasoData.setIdTipoImg(tipoImagen);
            insertImgCasoData.setImgB64(getImageBase64(imageToInsert));
            DecrimRequestInsertImgCasoBody insertImgCasoBody = new DecrimRequestInsertImgCasoBody();
            insertImgCasoBody.setInsertImgCasoData(insertImgCasoData);
            DecrimRequestInsertImgCasoEnvelope insertImgCasoEnvelope = new DecrimRequestInsertImgCasoEnvelope();
            insertImgCasoEnvelope.setBody(insertImgCasoBody);

            DecrimClientService clientService = retrofit.create(DecrimClientService.class);
            Call<DecrimResponseEnvelope> serviceResponse = clientService.insertImgCaso(insertImgCasoEnvelope);
            serviceResponse.enqueue(new Callback<DecrimResponseEnvelope>() {
                @Override
                public void onResponse(Call<DecrimResponseEnvelope> call, Response<DecrimResponseEnvelope> response) {
                    sendInsertImgCaso(idCaso, imagenes, callback);
                }

                @Override
                public void onFailure(Call<DecrimResponseEnvelope> call, Throwable t) {

                }
            });

        } else {
            callback.invalidateDrawable(null);
        }
    }

    public static void searchByIdCaso(DecrimRequestSearchByIdCaso searchByIdCaso, Callback<DecrimResponseEnvelopeSearchById> callback) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();

        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        Strategy strategy = new AnnotationStrategy();

        Serializer serializer = new Persister(strategy);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .connectTimeout(2, TimeUnit.MINUTES)
                .writeTimeout(2, TimeUnit.MINUTES)
                .readTimeout(2, TimeUnit.MINUTES)
                .build();

        Retrofit retrofit =  new Retrofit.Builder()
                .addConverterFactory(SimpleXmlConverterFactory.create(serializer))
                .baseUrl("http://servicios.identidadenlinea.com")
                .client(okHttpClient)
                .build();



        DecrimRequestSearchByIdCasoBody body = new DecrimRequestSearchByIdCasoBody();
        body.setSearchByIdCaso(searchByIdCaso);

        DecrimRequestSearchByIdCasoEnvelope requestEnvelope = new DecrimRequestSearchByIdCasoEnvelope();
        requestEnvelope.setBody(body);

        DecrimClientService clientService = retrofit.create(DecrimClientService.class);
        Call<DecrimResponseEnvelopeSearchById> serviceResponse = clientService.searchbyIdCaso(requestEnvelope);
        serviceResponse.enqueue(callback);
    }

    public static String getImageBase64(Bitmap bitmap) {
        ByteArrayOutputStream stream=new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
        byte[] image=stream.toByteArray();
        String img_str = Base64.encodeToString(image, Base64.DEFAULT);
        return img_str;
    }

    public static Bitmap getBitmapFromBase64(String base64Image) {
        byte[] decodedString = Base64.decode(base64Image, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }

    public static PerfilPersona parseCedulaInfo(String cedulaInfo) throws ParseException {
        if (cedulaInfo != null) {
            PerfilPersona cedulaData = new PerfilPersona(cedulaInfo);
            return cedulaData;
        } else {
            return null;
        }
    }

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public PerfilPersona() {
    }

    public PerfilPersona(String cedulaInfo) throws ParseException {
        if (cedulaInfo != null) {
            if (cedulaInfo.contains(NUEVO_FORMATO_CEDULA)) {
                actualFormatoCedula(cedulaInfo);
            } else {
                anteriorFormatoCedulaParsing(cedulaInfo);
            }
        }
    }

    private void actualFormatoCedula(String cedulaInfo) {
        char letra = cedulaInfo.charAt(0);
        String[] initialData = cedulaInfo.split(String.valueOf(letra));
        initialData = removeEmptyElements(initialData);
        //Slot 2 tiene Datos.
        if (initialData[2].replaceAll("[0-9]+", "").matches("[ÑA-Z]+")) {
            String[] dataDocumento = initialData[2].split("00");
            numeroDocumento = dataDocumento[1].replaceAll("[ÑA-Z]+", "");
            apellidos = dataDocumento[1].replaceAll("[0-9]+", "") + " " + initialData[3];
            nombres = initialData[4] + " " + initialData[5];
            sexo = initialData[6].substring(0, 2).replaceAll("[0-9]+", "");
            String fechaData = initialData[6].substring(2, 10);
            fechaNacimiento = fechaData.substring(4, 6) + "/" + fechaData.substring(6, 8) + "/" + fechaData.substring(0, 4);
            RHSanguineo = initialData[6].substring(initialData[6].length() - 2,  initialData[6].length());
        } else {
            String data = initialData[3];
            numeroDocumento = cedulaTrim(data.replaceAll("[A-Z]+", ""));
            apellidos = data.replaceAll("[0-9]+", "") + " " + initialData[4];
            nombres = initialData[5];
            sexo = initialData[6].substring(0, 2).replaceAll("[0-9]+", "");
            String fechaData = initialData[6].substring(2, 10);
            fechaNacimiento = fechaData.substring(4, 6) + "/" + fechaData.substring(6, 8) + "/" + fechaData.substring(0, 4);
            RHSanguineo = initialData[6].substring(initialData[6].length() - 2,  initialData[6].length());
        }
    }

    private String cedulaTrim(String cedula) {
        while (cedula.startsWith("0")) {
            cedula = cedula.substring(1, cedula.length());
        }
        return cedula;
    }

    private void anteriorFormatoCedulaParsing(String cedulaInfo) {
        cedulaInfo = trimCaracteresNoValidos(cedulaInfo);
        cedulaInfo = cedulaInfo.substring(50, 170);
        String[] initialData;
        initialData = cedulaInfo.split(" ");
        initialData = removeEmptyElements(initialData);

        numeroDocumento = initialData[0].replaceAll("[A-Z]+", "");
        apellidos = parsePrimerApellido(initialData[0]) + " " + initialData[1];
        nombres = initialData[2] + " " + (initialData[3].startsWith("0") ? "" : initialData[3]);
        String fechaNacimientoStr = "";
        if (initialData[3].startsWith("0")) {

            fechaNacimientoStr = initialData[3];

        } else {
            fechaNacimientoStr = initialData[4];
        }
        sexo = fechaNacimientoStr.substring(1, 2);
        String fecha = fechaNacimientoStr.substring(6, 8) + "/" + fechaNacimientoStr.substring(8, 10) + "/" + fechaNacimientoStr.substring(2, 6);
        fechaNacimiento = fecha;
        RHSanguineo = fechaNacimientoStr.substring(16, 18);
    }

    private String trimCaracteresNoValidos(String info) {
        char letra = info.charAt(0);
        return info.replaceAll(String.valueOf(letra), "");
    }

    private String parsePrimerApellido(String data) {
        return data.replaceAll("[0-9]+", "");
    }

    private String[] removeEmptyElements(String []data) {
        ArrayList<String> result = new ArrayList<>();
        for (String element : data) {
            if (!element.isEmpty()) {
                result.add(element);
            }
        }
        return result.toArray(new String[0]);
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public String getNombres() {
        return nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public String getRHSanguineo() {
        return RHSanguineo;
    }

    public String getSexo() {
        return sexo;
    }

    public int getTipoDocumento() {
        return tipoDocumento.equals("CC") ? 1 : 2;
    }

    public HuellaResponseData getHuellaResponseData() {
        return huellaResponseData;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public int getIdCaso() {
        return idCaso;
    }

    public void setIdCaso(int idCaso) {
        this.idCaso = idCaso;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public void setRHSanguineo(String RHSanguineo) {
        this.RHSanguineo = RHSanguineo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public void setHuellaResponseData(HuellaResponseData huellaResponseData) {
        this.huellaResponseData = huellaResponseData;
    }

    @NonNull
    public static String getUsernameDecrimService() {
        return "AM_Produccion";
    }

    public static String getPasswordDecrimService() {
        return Base64.encodeToString("AM_*Produccion*".getBytes(), Base64.DEFAULT).replace("\n", "");
    }


}

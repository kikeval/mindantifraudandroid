package com.antifraudminds.mindanatifraud.model;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by gustavo.bonilla on 10/16/2016.
 */
@Root(name = "searchbyIdCasoResponse", strict = false)
@Namespace(reference = "http://tempuri.org/")
public class DecrimSearchByIdCasoResponse {
    @ElementList(name = "searchbyIdCasoResult", required = false)
    private List<ConsultaResponse> searchByidCasoResult;

    public List<ConsultaResponse> getSearchByidCasoResult() {
        return searchByidCasoResult;
    }

    public void setSearchByidCasoResult(List<ConsultaResponse> searchByidCasoResult) {
        this.searchByidCasoResult = searchByidCasoResult;
    }
}

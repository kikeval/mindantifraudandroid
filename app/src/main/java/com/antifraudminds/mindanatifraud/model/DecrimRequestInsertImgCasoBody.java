package com.antifraudminds.mindanatifraud.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by gustavo.bonilla on 10/17/2016.
 */
@Root(name = "soap12:Body", strict = false)
public class DecrimRequestInsertImgCasoBody {
    @Element(name = "insertImgCaso",required = false)
    private DecrimRequestInsertImgCasoData insertImgCasoData;

    public DecrimRequestInsertImgCasoData getInsertImgCasoData() {
        return insertImgCasoData;
    }

    public void setInsertImgCasoData(DecrimRequestInsertImgCasoData insertImgCasoData) {
        this.insertImgCasoData = insertImgCasoData;
    }
}

package com.antifraudminds.mindanatifraud.model;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by gustavo.bonilla on 10/16/2016.
 */
@Root(name = "insertCasoResponse", strict = false)
@Namespace(reference = "http://tempuri.org/")
public class DecrimInsertCasoResponse {
    @ElementList(name = "insertCasoResult", required = false)
    private List<String> insertCasoResult;

    public List<String> getInsertCasoResult() {
        return insertCasoResult;
    }

    public void setInsertCasoResult(List<String> insertCasoResult) {
        this.insertCasoResult = insertCasoResult;
    }
}

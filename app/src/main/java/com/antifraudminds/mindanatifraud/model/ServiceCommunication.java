package com.antifraudminds.mindanatifraud.model;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by gustavo.bonilla on 8/07/2017.
 */

public class ServiceCommunication {
    public static String BASE_URL = "http://antifraudmindsweb-antifraudminds.b9ad.pro-us-east-1.openshiftapps.com/";
    public static Retrofit getRetrofitBuilder() {
        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(5, TimeUnit.MINUTES).readTimeout(5, TimeUnit.MINUTES).build();
        return new Retrofit.Builder().baseUrl(ServiceCommunication.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
    }
}

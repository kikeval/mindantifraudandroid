package com.antifraudminds.mindanatifraud.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by gustavo.bonilla on 10/16/2016.
 */
@Root(name = "soap12:Body", strict = false)
public class DecrimRequestInsertCasoBody {
    @Element(name = "insertCaso",required = false)
    private DecrimRequestInsertCasoData insertCasoData;

    public DecrimRequestInsertCasoData getInsertCasoData() {
        return insertCasoData;
    }

    public void setInsertCasoData(DecrimRequestInsertCasoData insertCasoData) {
        this.insertCasoData = insertCasoData;
    }
}

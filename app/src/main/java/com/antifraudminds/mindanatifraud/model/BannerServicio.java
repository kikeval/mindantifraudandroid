package com.antifraudminds.mindanatifraud.model;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

/**
 * Created by gustavo.bonilla on 11/6/2016.
 */

public interface BannerServicio {

    @Headers({
            "Content-Type: application/json",
            "User-Agent: AndroidAntiMindApp"
    })
    @GET("/banners/all/ban")
    Call<ResponseManagerBanner> getBanner();
}

package com.antifraudminds.mindanatifraud.model;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

/**
 * Created by gustavo.bonilla on 4/08/2017.
 */

public class Licencia {
    private String license;
    private String expiration;

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getExpiration() {
        return expiration;
    }

    public void setExpiration(String expiration) {
        this.expiration = expiration;
    }

    public static void getLicencia(Callback<ResponseManagerLicencia> callback) {
        Retrofit retrofit = ServiceCommunication.getRetrofitBuilder();
        LicenciaServicio servicio = retrofit.create(LicenciaServicio.class);
        Call<ResponseManagerLicencia> licencia = servicio.getLicencia();
        licencia.enqueue(callback);
    }
}

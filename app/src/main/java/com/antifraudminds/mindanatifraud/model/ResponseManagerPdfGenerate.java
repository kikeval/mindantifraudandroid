package com.antifraudminds.mindanatifraud.model;

/**
 * Created by gustavo.bonilla on 12/11/2016.
 */
public class ResponseManagerPdfGenerate {
    public static final String NO_ERROR = "NO_ERROR";
    private Object manager;
    protected String object;
    private String mensaje;
    private Object error;

    public Object getManager() {
        return manager;
    }

    public void setManager(Object manager) {
        this.manager = manager;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Object getError() {
        return error;
    }

    public void setError(Object error) {
        this.error = error;
    }
}

package com.antifraudminds.mindanatifraud.model;

import java.util.List;

/**
 * Created by gustavo.bonilla on 11/6/2016.
 */

public class ResponseManagerLicencia {

    public static final String NO_ERROR = "NO_ERROR";
    private Object manager;
    protected Licencia object;
    private String mensaje;
    private Object error;

    public Object getManager() {
        return manager;
    }

    public Licencia getObject() {
        return object;
    }

    public String getMensaje() {
        return mensaje;
    }

    public Object getError() {
        return error;
    }
}

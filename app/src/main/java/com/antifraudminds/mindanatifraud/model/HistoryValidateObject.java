package com.antifraudminds.mindanatifraud.model;

import android.content.SharedPreferences;
import android.graphics.Bitmap;

import com.antifraudminds.mindanatifraud.adapter.HistoryValidateAdapter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kikeval on 3/09/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class HistoryValidateObject {

    public final static String KEY_HISTORY = "Key_History";

    public String nameValidate;
    public String identification;
    public String imgStatus;
    public String idCaso;
    @JsonIgnore
    private Bitmap imageBitmap;

    public HistoryValidateObject() {
    }

    public HistoryValidateObject (String nameValidate,String identification,String imgStatus){

        this.nameValidate = nameValidate;
        this.identification = identification;
        this.imgStatus = imgStatus;
    }

    public static List<HistoryValidateObject> getHistoryValidateData(SharedPreferences sharedPreferences) throws IOException {
        String jsonHistoryString = sharedPreferences.getString(KEY_HISTORY, "[]");
        if (jsonHistoryString.equals("[]")) {
            return new ArrayList<>();
        } else {
            ObjectMapper mapper = new ObjectMapper();
            ObjectReader reader = mapper.reader(new TypeReference<List<HistoryValidateObject>>() {});
            List<HistoryValidateAdapter> list = reader.readValue(jsonHistoryString);
            return (List<HistoryValidateObject>) (list != null ? list : new ArrayList<>());
        }
    }

    public void setImageBitmap(Bitmap bitmap) {
        this.imageBitmap = bitmap;
    }

    public Bitmap getImageBitmap() {
        if (imageBitmap == null) {
            imageBitmap = PerfilPersona.getBitmapFromBase64(imgStatus);
        }
        return imageBitmap;
    }

    public boolean isUrlImage() {
        return imgStatus.contains("/upload");
    }

    public static void saveHistoryValidateData(HistoryValidateObject historyValidateObject, SharedPreferences.Editor editor, SharedPreferences sharedPreferences) throws IOException {
        List<HistoryValidateObject> historyValidateObjectList = getHistoryValidateData(sharedPreferences);
        historyValidateObjectList.add(historyValidateObject);
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        String jsonHistoryString = mapper.writeValueAsString(historyValidateObjectList);
        editor.putString(KEY_HISTORY, jsonHistoryString);
        editor.commit();
    }


}

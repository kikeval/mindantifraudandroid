package com.antifraudminds.mindanatifraud.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.NamespaceList;
import org.simpleframework.xml.Root;

/**
 * Created by gustavo.bonilla on 10/17/2016.
 */
@Root(name = "soap12:Envelope")
@NamespaceList({
        @Namespace( prefix = "xsi", reference = "http://www.w3.org/2001/XMLSchema-instance"),
        @Namespace( prefix = "xsd", reference = "http://www.w3.org/2001/XMLSchema"),
        @Namespace( prefix = "soap12", reference = "http://www.w3.org/2003/05/soap-envelope")
})
public class DecrimRequestInsertImgCasoEnvelope {
    @Element(name = "soap12:Body", required = false)
    private DecrimRequestInsertImgCasoBody body;

    public DecrimRequestInsertImgCasoBody getBody() {
        return body;
    }

    public void setBody(DecrimRequestInsertImgCasoBody body) {
        this.body = body;
    }
}

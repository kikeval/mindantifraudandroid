package com.antifraudminds.mindanatifraud.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by gustavo.bonilla on 10/17/2016.
 */
@Root(name = "soap12:Body", strict = false)
public class DecrimRequestSearchByIdCasoBody {
    @Element(name = "searchbyIdCaso",required = false)
    private DecrimRequestSearchByIdCaso insertImgCasoData;

    public DecrimRequestSearchByIdCaso getSearchByIdCaso() {
        return insertImgCasoData;
    }

    public void setSearchByIdCaso(DecrimRequestSearchByIdCaso insertImgCasoData) {
        this.insertImgCasoData = insertImgCasoData;
    }
}

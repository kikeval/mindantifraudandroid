package com.antifraudminds.mindanatifraud.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by gustavo.bonilla on 10/16/2016.
 */
@Root(name = "Body", strict = false)
public class DecrimInsertCaseResponseBody {
    @Element(name = "insertCasoResponse", required = false)
    private DecrimInsertCasoResponse insertCasoResponse;

    public DecrimInsertCasoResponse getInsertCasoResponse() {
        return insertCasoResponse;
    }

    public void setInsertCasoResponse(DecrimInsertCasoResponse insertCasoResponse) {
        this.insertCasoResponse = insertCasoResponse;
    }
}

package com.antifraudminds.mindanatifraud.model;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;

/**
 * Created by gustavo.bonilla on 11/30/2016.
 */

public interface SolicitudServicio {
    @Multipart
    @PUT("solicitud")
    Call<ResponseManager> solicitud(@Part("idEmpresa") RequestBody idEmpresa,
                                    @Part("idServicio") RequestBody  idServicio,
                                    @Part("tituloSolicitud") RequestBody  tituloSolicitud,
                                    @Part("txtRequerimiento") RequestBody  txtRequerimiento,
                                    @Part("idUsuario") RequestBody  idUsuario,
                                    @Part List<MultipartBody.Part> files);

    @Headers({
            "Content-Type: application/json",
            "User-Agent: AndroidAntiMindApp"
    })
    @PUT("solicitud/nofile")
    Call<ResponseManager> solicitudNoFile(@Body Solicitud usuario);
}

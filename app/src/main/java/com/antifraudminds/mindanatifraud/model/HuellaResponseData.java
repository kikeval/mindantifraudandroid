package com.antifraudminds.mindanatifraud.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by gustavo.bonilla on 10/16/2016.
 */

public class HuellaResponseData implements Parcelable {
    private byte []data;
    private String huellaBitmap;

    public HuellaResponseData(byte[] data, Bitmap huellaBitmap) {
        this.data = data;
        this.huellaBitmap = saveToFile(huellaBitmap);
    }

    protected HuellaResponseData(Parcel in) {
        data = in.createByteArray();
        huellaBitmap = in.readString();
    }

    public static final Creator<HuellaResponseData> CREATOR = new Creator<HuellaResponseData>() {
        @Override
        public HuellaResponseData createFromParcel(Parcel in) {
            return new HuellaResponseData(in);
        }

        @Override
        public HuellaResponseData[] newArray(int size) {
            return new HuellaResponseData[size];
        }
    };

    public byte[] getData() {
        return data;
    }

    public Bitmap getHuellaBitmap() {
        return BitmapFactory.decodeFile(huellaBitmap);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByteArray(data);
        dest.writeString(huellaBitmap);
    }

    private String saveToFile(Bitmap bitmap) {
        String fileName = Environment.getExternalStorageDirectory() + "/Huella" + Math.round(Math.random() * 1000) + ".png";
        try {
            File file = new File(fileName);
            FileOutputStream fOut = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 85, fOut);
            fOut.flush();
            fOut.close();
        } catch (Exception exp) {

        }
        return fileName;
    }
}

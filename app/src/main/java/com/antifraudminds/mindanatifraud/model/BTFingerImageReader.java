package com.antifraudminds.mindanatifraud.model;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.antifraudminds.mindanatifraud.service.BluetoothReaderService;
import com.fgtit.data.fpimage;
import com.fgtit.data.wsq;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Class created to handle the read for Image of the FingerPrint.
 * Clase creada para manejar la lectura de la Imagen de la huella.
 *
 * @author gustavo.bonilla
 */
public class BTFingerImageReader {

    //region Constantes de la clase
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    private final static byte CMD_GETIMAGE=0x30;
    private final static String TAG = BTFingerImageReader.class.getSimpleName();
    //endregion

    //region Attributos privados
    private BluetoothAdapter mBluetoothAdapter = null;
    private BluetoothReaderService mBTService = null;
    private Context mContext;
    private int mBTState;
    private FingerImageReaderListener readerListener;
    private int mUpImageSize=0;
    private byte mUpImage[]=new byte[73728];//36864
    private boolean mIsWork;
    private Timer mTimerTimeout;
    private Handler mHandlerTimeout;
    private TimerTask mTaskTimeout;
    private Bitmap mImage;

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_STATE_CHANGE:
                    mBTState = msg.arg1;
                    if (readerListener != null) {
                        readerListener.stateChanged(mBTState);
                    }
                    break;
                case MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    ReceiveCommand(readBuf,msg.arg1);
                    if (!mIsWork) {
                        HuellaResponseData responseData = new HuellaResponseData(readBuf, Bitmap.createBitmap(mImage));
                        mImage = null;
                        if (readerListener != null) {
                            readerListener.imageCaptured(responseData);
                        }
                    }

                    break;
                case MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    break;
            }
        }
    };
    //endregion

    private void saveBitmap(Bitmap bitmap) throws IOException {
        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/huella.jpg");
        FileOutputStream fOut = new FileOutputStream(file);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
        fOut.flush();
        fOut.close();
    }

    /**
     * Interfaz que define los listeners para distintos eventos del sensor.
     */
    public interface FingerImageReaderListener {
        void stateChanged(int state);
        void imageCaptured(HuellaResponseData data);
    }

    /**
     * Constructor.
     *
     * @param context  contexto de la aplicación ó activity
     * @param listener Listener de callback para las acciones del sensor.
     */
    public BTFingerImageReader(Context context, FingerImageReaderListener listener) {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        this.mContext = context;
        this.readerListener = listener;
    }


    //region Métodos Privados

    private void ReceiveCommand(byte[] databuf, int datasize) {

        memcpy(mUpImage, mUpImageSize, databuf, 0, datasize);
        mUpImageSize = mUpImageSize + datasize;
        Log.d(TAG, "mUpImageSize:" + mUpImageSize);
        if (mUpImageSize >= 15200) {
            byte[] opImage = new byte[256 * 288];
            fpimage.getInstance().ToStdImage(mUpImage, opImage);
            byte[] bmpdata = toBmpByte(256, 288, opImage);
            mImage = BitmapFactory.decodeByteArray(bmpdata, 0, bmpdata.length);

            //PNG
           /* try {
                if (mImage != null) {
                    SavePngFile(mImage,"fp_tavo.png");
                    saveBitmap(mImage);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            */

            //WSQ
            //byte[] inpdata=new byte[73728];
            //int inpsize=73728;
            //System.arraycopy(bmpdata,1078, inpdata, 0, inpsize);
            //SaveWsqFile(inpdata,inpsize,"fp.wsq");

            mUpImageSize = 0;
            mIsWork = false;

        }
    }

    public void SavePngFile(Bitmap bmp,String filename){
        File f = new File(Environment.getExternalStorageDirectory()+"/"+filename);
        if (f.exists()) {
            f.delete();
        }
        try {
            FileOutputStream out = new FileOutputStream(f);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.flush();
            out.close();
        } catch (IOException e) {
        }
    }

    private void memcpy(byte[] dstbuf,int dstoffset,byte[] srcbuf,int srcoffset,int size) {
        for(int i=0;i<size;i++) {
            dstbuf[dstoffset+i]=srcbuf[srcoffset+i];
        }
    }

    public void SaveWsqFile(byte[] rawdata,int rawsize,String filename){
        byte[] outdata=new byte[73728];
        int[] outsize=new int[1];
        wsq.getInstance().RawToWsq(rawdata,rawsize,256,288, outdata, outsize,2.833755f);
        try {
            File fs=new File(Environment.getExternalStorageDirectory()+"/"+filename);
            if(fs.exists()){
                fs.delete();
            }
            new File(Environment.getExternalStorageDirectory()+"/"+filename);
            RandomAccessFile randomFile = new RandomAccessFile(Environment.getExternalStorageDirectory()+"/"+filename, "rw");
            long fileLength = randomFile.length();
            randomFile.seek(fileLength);
            randomFile.write(outdata, 0, outsize[0]);
            randomFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private byte[] getFingerprintImage(byte[] data,int width,int height) {
        if (data == null) {
            return null;
        }
        byte[] imageData = new byte[data.length * 2];
        for (int i = 0; i < data.length; i++) {
            imageData[i * 2] = (byte) (data[i] & 0xf0);
            imageData[i * 2 + 1] = (byte) (data[i] << 4 & 0xf0);
        }
        byte[] bmpData = toBmpByte(width, height, imageData);
        return bmpData;
    }

    private byte[] changeByte(int data) {
        byte b4 = (byte) ((data) >> 24);
        byte b3 = (byte) (((data) << 8) >> 24);
        byte b2 = (byte) (((data) << 16) >> 24);
        byte b1 = (byte) (((data) << 24) >> 24);
        byte[] bytes = { b1, b2, b3, b4 };
        return bytes;
    }

    private byte[] toBmpByte(int width, int height, byte[] data) {
        byte[] buffer = null;
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            DataOutputStream dos = new DataOutputStream(baos);

            int bfType = 0x424d;
            int bfSize = 54 + 1024 + width * height;
            int bfReserved1 = 0;
            int bfReserved2 = 0;
            int bfOffBits = 54 + 1024;

            dos.writeShort(bfType);
            dos.write(changeByte(bfSize), 0, 4);
            dos.write(changeByte(bfReserved1), 0, 2);
            dos.write(changeByte(bfReserved2), 0, 2);
            dos.write(changeByte(bfOffBits), 0, 4);

            int biSize = 40;
            int biWidth = width;
            int biHeight = height;
            int biPlanes = 1;
            int biBitcount = 8;
            int biCompression = 0;
            int biSizeImage = width * height;
            int biXPelsPerMeter = 0;
            int biYPelsPerMeter = 0;
            int biClrUsed = 256;
            int biClrImportant = 0;

            dos.write(changeByte(biSize), 0, 4);
            dos.write(changeByte(biWidth), 0, 4);
            dos.write(changeByte(biHeight), 0, 4);
            dos.write(changeByte(biPlanes), 0, 2);
            dos.write(changeByte(biBitcount), 0, 2);
            dos.write(changeByte(biCompression), 0, 4);
            dos.write(changeByte(biSizeImage), 0, 4);
            dos.write(changeByte(biXPelsPerMeter), 0, 4);
            dos.write(changeByte(biYPelsPerMeter), 0, 4);
            dos.write(changeByte(biClrUsed), 0, 4);
            dos.write(changeByte(biClrImportant), 0, 4);

            byte[] palatte = new byte[1024];
            for (int i = 0; i < 256; i++) {
                palatte[i * 4] = (byte) i;
                palatte[i * 4 + 1] = (byte) i;
                palatte[i * 4 + 2] = (byte) i;
                palatte[i * 4 + 3] = 0;
            }
            dos.write(palatte);

            dos.write(data);
            dos.flush();
            buffer = baos.toByteArray();
            dos.close();
            baos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return buffer;
    }

    public void setBTService() {
        mBTService = new BluetoothReaderService(mContext, mHandler);
    }

    public int getServiceState() {
        return mBTService != null ? mBTService.getState() : -1;
    }

    private int calcCheckSum(byte[] buffer,int size) {
        int sum=0;
        for(int i=0;i<size;i++) {
            sum=sum+buffer[i];
        }
        return (sum & 0x00ff);
    }

    private void TimeOutStart() {
        if (mTimerTimeout != null) {
            return;
        }
        mTimerTimeout = new Timer();
        mHandlerTimeout = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                TimeOutStop();
                if(mIsWork){
                    mIsWork=false;
                }
                super.handleMessage(msg);
            }
        };
        mTaskTimeout = new TimerTask() {
            @Override
            public void run() {
                Message message = new Message();
                message.what = 1;
                mHandlerTimeout.sendMessage(message);
            }
        };
        mTimerTimeout.schedule(mTaskTimeout, 10000, 10000);
    }

    private void TimeOutStop() {
        if (mTimerTimeout!=null) {
            mTimerTimeout.cancel();
            mTimerTimeout = null;
            mTaskTimeout.cancel();
            mTaskTimeout=null;
        }
    }
    //endregion

    //region Métodos Públicos
    public void connect(BluetoothDevice device) {
        if (mBTService == null) {
            setBTService();
        }
        mBTService.connect(device);
    }

    public void sendCommand() {
        if (mIsWork)
            return;

        int sendsize = 9;
        byte[] sendbuf = new byte[sendsize];
        sendbuf[0] = 'F';
        sendbuf[1] = 'T';
        sendbuf[2] = 0;
        sendbuf[3] = 0;
        sendbuf[4] = CMD_GETIMAGE;
        sendbuf[5] = (byte) (0);
        sendbuf[6] = (byte) (0 >> 8);

        int sum = calcCheckSum(sendbuf, 7);
        sendbuf[7] = (byte) (sum);
        sendbuf[8] = (byte) (sum >> 8);

        mIsWork = true;
        mUpImageSize = 0;
        mBTService.write(sendbuf);
    }

    public int getBTState() {
        return mBTState;
    }

    public void stopService() {
        mBTService.stop();
    }

    public Handler getHandler() {
        return mHandler;
    }
    //endregion
}

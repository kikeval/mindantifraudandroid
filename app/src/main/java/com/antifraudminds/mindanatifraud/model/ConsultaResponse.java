package com.antifraudminds.mindanatifraud.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

/**
 * Created by gustavo.bonilla on 10/16/2016.
 */
@Root(name = "searchbyIdCasoResult", strict = false)
@Namespace(reference = "http://tempuri.org/")
public class ConsultaResponse {
    @Element(name = "idCaso", required = false)
    private String idCaso;
    @Element(name = "mensaje", required = false)
    private String mensaje;
    @Element(name = "estado", required = false)
    private String estado;
    @Element(name = "resultadoValidacion", required = false)
    private String resultadoValidacion;
    @Element(name = "resultadoIdentificacion", required = false)
    private String resultadoIdentificacion;
    @Element(name = "resultadoHuella", required = false)
    private String resultadoHuella;
    @Element(name = "observaciones", required = false)
    private String observaciones;
    @Element(name = "fechaHoraCreacion", required = false)
    private String fechaHoraCreacion;
    @Element(name = "fechaHoraEvaluacion", required = false)
    private String fechaHoraEvaluacion;
    @Element(name = "tipoIdentificacion", required = false)
    private String tipoIdentificacion;
    @Element(name = "numeroIdentificacion", required = false)
    private String numeroIdentificacion;
    @Element(name = "pApellido", required = false)
    private String pApellido;
    @Element(name = "sApellido", required = false)
    private String sApellido;
    @Element(name = "pNombre", required = false)
    private String pNombre;
    @Element(name = "sNombre", required = false)
    private String sNombre;

    public String getIdCaso() {
        return idCaso;
    }

    public void setIdCaso(String idCaso) {
        this.idCaso = idCaso;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getResultadoValidacion() {
        return resultadoValidacion;
    }

    public void setResultadoValidacion(String resultadoValidacion) {
        this.resultadoValidacion = resultadoValidacion;
    }

    public String getResultadoIdentificacion() {
        return resultadoIdentificacion;
    }

    public void setResultadoIdentificacion(String resultadoIdentificacion) {
        this.resultadoIdentificacion = resultadoIdentificacion;
    }

    public String getResultadoHuella() {
        return resultadoHuella;
    }

    public void setResultadoHuella(String resultadoHuella) {
        this.resultadoHuella = resultadoHuella;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getFechaHoraCreacion() {
        return fechaHoraCreacion;
    }

    public void setFechaHoraCreacion(String fechaHoraCreacion) {
        this.fechaHoraCreacion = fechaHoraCreacion;
    }

    public String getFechaHoraEvaluacion() {
        return fechaHoraEvaluacion;
    }

    public void setFechaHoraEvaluacion(String fechaHoraEvaluacion) {
        this.fechaHoraEvaluacion = fechaHoraEvaluacion;
    }

    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    public String getpApellido() {
        return pApellido;
    }

    public void setpApellido(String pApellido) {
        this.pApellido = pApellido;
    }

    public String getsApellido() {
        return sApellido;
    }

    public void setsApellido(String sApellido) {
        this.sApellido = sApellido;
    }

    public String getpNombre() {
        return pNombre;
    }

    public void setpNombre(String pNombre) {
        this.pNombre = pNombre;
    }

    public String getsNombre() {
        return sNombre;
    }

    public void setsNombre(String sNombre) {
        this.sNombre = sNombre;
    }

    public void insertarValidacion(String resultadoBaseDeDatos, Callback<ResponseManager> callback) {
        Retrofit retrofit = ServiceCommunication.getRetrofitBuilder();
        DecrimResultadoValidacion resultadoValidacion = new DecrimResultadoValidacion(getIdCaso(), getResultadoValidacion(),
                getResultadoIdentificacion(), getResultadoHuella(), resultadoBaseDeDatos);
        DecrimServicio decrimServicio = retrofit.create(DecrimServicio.class);
        Call<ResponseManager> auth = decrimServicio.insertarResultadoValidacion(resultadoValidacion);
        auth.enqueue(callback);

    }

    public class DecrimResultadoValidacion {
        private String idCaso;
        private String resultadoValidacion;
        private String resultadoIdentificacion;
        private String resultadoHuella;
        private String resultadoBasedeDatos;

        public DecrimResultadoValidacion(String idCaso,
                String resultadoValidacion,
                String resultadoIdentificacion,
                String resultadoHuella,
                String resultadoBasedeDatos) {
            this.idCaso = idCaso;
            this.resultadoValidacion = resultadoValidacion;
            this.resultadoIdentificacion = resultadoIdentificacion;
            this.resultadoHuella = resultadoHuella;
            this.resultadoBasedeDatos = resultadoBasedeDatos;
        }

        public String getIdCaso() {
            return idCaso;
        }

        public void setIdCaso(String idCaso) {
            this.idCaso = idCaso;
        }

        public String getResultadoValidacion() {
            return resultadoValidacion;
        }

        public void setResultadoValidacion(String resultadoValidacion) {
            this.resultadoValidacion = resultadoValidacion;
        }

        public String getResultadoIdentificacion() {
            return resultadoIdentificacion;
        }

        public void setResultadoIdentificacion(String resultadoIdentificacion) {
            this.resultadoIdentificacion = resultadoIdentificacion;
        }

        public String getResultadoHuella() {
            return resultadoHuella;
        }

        public void setResultadoHuella(String resultadoHuella) {
            this.resultadoHuella = resultadoHuella;
        }

        public String getResultadoBasedeDatos() {
            return resultadoBasedeDatos;
        }

        public void setResultadoBasedeDatos(String resultadoBasedeDatos) {
            this.resultadoBasedeDatos = resultadoBasedeDatos;
        }
    }
}

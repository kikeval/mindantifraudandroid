package com.antifraudminds.mindanatifraud.model;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by gustavo.bonilla on 12/2/2016.
 */

public interface DecrimServicio {
    @Headers({
            "Content-Type: application/json",
            "User-Agent: AndroidAntiMindApp"
    })
    @PUT("/decrimdata/insert")
    Call<ResponseManager> insertar(@Body DecrimData decrimData);

    @Headers({
            "Content-Type: application/json",
            "User-Agent: AndroidAntiMindApp"
    })
    @PUT("/decrimdata/file")
    Call<ResponseManager> archivo(@Body DecrimDataArchivos decrimDataArchivo);

    @Headers({
            "Content-Type: application/json",
            "User-Agent: AndroidAntiMindApp"
    })
    @GET("/decrimdata/getall/{idEmpresa}")
    Call<ResponseManagerDecrimData> getAll(@Path("idEmpresa") String idEmpresa);

    @Headers({
        "Content-Type: application/json",
        "User-Agent: AndroidAntiMindApp"
    })
    @GET("/decrimdata/get/{idCaso}")
    Call<ResponseManagerDecrimData> getByIdCaso(@Path("idCaso") String idCaso);

    @Headers({
            "Content-Type: application/json",
            "User-Agent: AndroidAntiMindApp"
    })
    @POST("/decrimdata/getresultlistanegra")
    Call<ResponseManagerListaNegra> getResultListaNegra(@Body ListaNegraData listaNegraData);

    @Headers({
            "Content-Type: application/json",
            "User-Agent: AndroidAntiMindApp"
    })
    @POST("/decrimdata/createpdf")
    Call<ResponseManagerPdfGenerate> generatePdfFile(@Body PdfFileRequest listaNegraData);

    @Headers({
            "Content-Type: application/json",
            "User-Agent: AndroidAntiMindApp"
    })
    @POST("/decrimdata/insertarresultadovalidacion")
    Call<ResponseManager> insertarResultadoValidacion(@Body ConsultaResponse.DecrimResultadoValidacion resultado);
}

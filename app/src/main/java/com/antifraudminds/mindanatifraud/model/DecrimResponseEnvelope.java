package com.antifraudminds.mindanatifraud.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.NamespaceList;
import org.simpleframework.xml.Root;

/**
 * Created by gustavo.bonilla on 10/16/2016.
 */
@Root(name = "soap:Envelope", strict = false)
@NamespaceList({
        @Namespace( prefix = "soap", reference = "http://www.w3.org/2003/05/soap-envelope"),
        @Namespace( prefix = "xsi", reference = "http://www.w3.org/2001/XMLSchema-instance"),
        @Namespace( prefix = "xsd", reference = "http://www.w3.org/2001/XMLSchema")
})
public class DecrimResponseEnvelope {
    @Element(name = "Body", required = false)
    private DecrimInsertCaseResponseBody body;

    public DecrimInsertCaseResponseBody getBody() {
        return body;
    }

    public void setBody(DecrimInsertCaseResponseBody body) {
        this.body = body;
    }
}

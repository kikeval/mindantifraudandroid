package com.antifraudminds.mindanatifraud.model;

import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by gustavo.bonilla on 12/4/2016.
 */

public class DecrimDataArchivos {
    private int idCaso;
    private String nombre;
    private String archivoUrl;
    private Date fechaCreacion;

    public DecrimDataArchivos() {
        fechaCreacion = new Date();
    }

    public int getIdCaso() {
        return idCaso;
    }

    public void setIdCaso(int idCaso) {
        this.idCaso = idCaso;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getArchivoUrl() {
        return archivoUrl;
    }

    public void setArchivoUrl(String archivoUrl) {
        this.archivoUrl = archivoUrl;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public static void sendDecrimDataArchivo(DecrimDataArchivos decrimData, Callback<ResponseManager> callback) {
        Retrofit retrofit = ServiceCommunication.getRetrofitBuilder();
        DecrimServicio decrimServicio = retrofit.create(DecrimServicio.class);
        Call<ResponseManager> auth = decrimServicio.archivo(decrimData);
        auth.enqueue(callback);
    }
}

package com.antifraudminds.mindanatifraud.model;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

/**
 * Created by gustavo.bonilla on 12/11/2016.
 */
public class PdfFileRequest {

    private String idCaso;
    private String resultadoValidacion;
    private String resultadoIdentificacion;
    private String resultadoHuella;
    private String listaNegra;
    private String porcentajeRiesgo;

    public void setIdCaso(String idCaso) {
        this.idCaso = idCaso;
    }

    public void setResultadoValidacion(String resultadoValidacion) {
        this.resultadoValidacion = resultadoValidacion;
    }

    public String getResultadoValidacion() {
        return resultadoValidacion;
    }

    public void setResultadoIdentificacion(String resultadoIdentificacion) {
        this.resultadoIdentificacion = resultadoIdentificacion;
    }

    public void setResultadoHuella(String resultadoHuella) {
        this.resultadoHuella = resultadoHuella;
    }

    public void setListaNegra(String listaNegra) {
        this.listaNegra = listaNegra;
    }

    public void setPorcentajeRiesgo(String porcentajeRiesgo) {
        this.porcentajeRiesgo = porcentajeRiesgo;
    }

    public static void generatePDFFile(PdfFileRequest fileRequest, Callback<ResponseManagerPdfGenerate> callback) {
        Retrofit retrofit = ServiceCommunication.getRetrofitBuilder();
        DecrimServicio decrimServicio = retrofit.create(DecrimServicio.class);
        Call<ResponseManagerPdfGenerate> auth = decrimServicio.generatePdfFile(fileRequest);
        auth.enqueue(callback);
    }
}

package com.antifraudminds.mindanatifraud.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by gustavo.bonilla on 10/16/2016.
 */
@Root(name = "Body", strict = false)
public class DecrimSearchbyIdCasoResponseBody {
    @Element(name = "searchbyIdCasoResponse", required = false)
    private DecrimSearchByIdCasoResponse searchByIdCasoResponse;

    public DecrimSearchByIdCasoResponse getSearchByIdCasoResponse() {
        return searchByIdCasoResponse;
    }

    public void setSearchByIdCasoResponse(DecrimSearchByIdCasoResponse searchByIdCasoResponse) {
        this.searchByIdCasoResponse = searchByIdCasoResponse;
    }
}

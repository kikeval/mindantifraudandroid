package com.antifraudminds.mindanatifraud.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

/**
 * Created by gustavo.bonilla on 10/17/2016.
 */
@Root(name = "insertImgCaso", strict = false)
@Namespace(reference = "http://tempuri.org/")
public class DecrimRequestInsertImgCasoData {
    @Element(name = "environment", required = false)
    private int environtment;
    @Element(name = "userName", required = false)
    private String userName;
    @Element(name = "password", required = false)
    private String password;
    @Element(name = "idCaso", required = false)
    private int idCaso;
    @Element(name = "idTipoImg", required = false)
    private int idTipoImg;
    @Element(name = "imgB64", required = false)
    private String imgB64;

    public int getEnvirontment() {
        return environtment;
    }

    public void setEnvirontment(int environtment) {
        this.environtment = environtment;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getIdCaso() {
        return idCaso;
    }

    public void setIdCaso(int idCaso) {
        this.idCaso = idCaso;
    }

    public int getIdTipoImg() {
        return idTipoImg;
    }

    public void setIdTipoImg(int idTipoImg) {
        this.idTipoImg = idTipoImg;
    }

    public String getImgB64() {
        return imgB64;
    }

    public void setImgB64(String imgB64) {
        this.imgB64 = imgB64;
    }
}

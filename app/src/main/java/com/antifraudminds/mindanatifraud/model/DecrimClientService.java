package com.antifraudminds.mindanatifraud.model;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by gustavo.bonilla on 10/16/2016.
 */

public interface DecrimClientService {
    @Headers({
            "Content-Type: application/soap+xml; charset=utf-8",
            "Accept-Charset: utf-8"
    })
    @POST("/client/decrimclientservice.asmx")
    Call<DecrimResponseEnvelope> insertCaso(@Body DecrimRequestInsertCasoEnvelope body);

    @Headers({
            "Content-Type: application/soap+xml; charset=utf-8",
            "Accept-Charset: utf-8"
    })
    @POST("/client/decrimclientservice.asmx")
    Call<DecrimResponseEnvelope> insertImgCaso(@Body DecrimRequestInsertImgCasoEnvelope body);

    @Headers({
            "Content-Type: application/soap+xml; charset=utf-8",
            "Accept-Charset: utf-8"
    })
    @POST("/client/decrimclientservice.asmx")
    Call<DecrimResponseEnvelopeSearchById> searchbyIdCaso(@Body DecrimRequestSearchByIdCasoEnvelope body);
}

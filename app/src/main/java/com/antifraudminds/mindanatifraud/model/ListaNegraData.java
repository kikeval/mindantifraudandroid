package com.antifraudminds.mindanatifraud.model;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.antifraudminds.mindanatifraud.model.ListaNegraData.TipoListaNegra.CAPTURAS;

/**
 * Created by gustavo.bonilla on 12/5/2016.
 */

public class ListaNegraData {

    private String TipoDoc;
    private String NumDoc;
    private String NombreCompleto;
    private String Ubicacion;
    private String Descrip1;
    private String Descrip2;
    private int tipo;
    private String fecha;

    public enum TipoListaNegra {
        NINGUNO(-1),
        CAPTURAS(1),
        DENUNCIAS(2),
        REINSERTADOS(3);
        private int tipo;
        TipoListaNegra(int tipo) {
            this.tipo = tipo;
        }

        public static TipoListaNegra getTipoListaNegra(int tipo) {
            for (TipoListaNegra tipoListaNegra : TipoListaNegra.values()) {
                if (tipoListaNegra.getTipo() == tipo) {
                    return tipoListaNegra;
                }
            }
            return NINGUNO;
        }

        public int getTipo() {
            return tipo;
        }
    }

    public ListaNegraData() {
    }

    public String getTipoDoc() {
        return TipoDoc;
    }

    public void setTipoDoc(String tipoDoc) {
        TipoDoc = tipoDoc;
    }

    public String getNumDoc() {
        return NumDoc;
    }

    public void setNumDoc(String numDoc) {
        NumDoc = numDoc;
    }

    public String getNombreCompleto() {
        return NombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        NombreCompleto = nombreCompleto;
    }

    public String getUbicacion() {
        return Ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        Ubicacion = ubicacion;
    }

    public String getDescrip1() {
        return Descrip1;
    }

    public void setDescrip1(String descrip1) {
        Descrip1 = descrip1;
    }

    public String getDescrip2() {
        return Descrip2;
    }

    public void setDescrip2(String descrip2) {
        Descrip2 = descrip2;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getTipo() {
        return tipo;
    }

    public static void findResults(ListaNegraData listaNegraData, Callback<ResponseManagerListaNegra> callback) {
        Retrofit retrofit = ServiceCommunication.getRetrofitBuilder();
        DecrimServicio decrimServicio = retrofit.create(DecrimServicio.class);
        Call<ResponseManagerListaNegra> getAll = decrimServicio.getResultListaNegra(listaNegraData);
        getAll.enqueue(callback);
    }
}

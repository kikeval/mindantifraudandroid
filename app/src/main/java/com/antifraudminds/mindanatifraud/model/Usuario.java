package com.antifraudminds.mindanatifraud.model;

import android.content.SharedPreferences;

import com.antifraudminds.mindanatifraud.adapter.HistoryValidateAdapter;
import com.antifraudminds.mindanatifraud.views.LoadingDialog;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by gustavo.bonilla on 10/16/2016.
 */

public class Usuario {
    private int id;
    private String nombre;
    private String email;
    private String password;
    private String nit;
    private String ciudad;
    private String logo;
    private int idUsuario;
    private int tipo;

    public static String KEY_USUARIO = "USUARIO";


    public Usuario() {
    }

    public static void authUser(final Usuario usuario, Callback<ResponseManager> callback) {

        Retrofit retrofit = ServiceCommunication.getRetrofitBuilder();
        UsuarioServicio usuarioServicio = retrofit.create(UsuarioServicio.class);
        Call<ResponseManager> auth = usuarioServicio.authUser(usuario);
        auth.enqueue(callback);
    }

    public static void saveUserLoginData(Usuario usuario, SharedPreferences.Editor editor) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        String jsonUsuarioString = mapper.writeValueAsString(usuario);
        editor.putString(UsuarioServicio.ALL_DATA, jsonUsuarioString);
        editor.commit();
    }

    public static Usuario retrieveUsuarioLoginData(SharedPreferences sharedPreferences) throws IOException {
        String jsonUsuarioString = sharedPreferences.getString(UsuarioServicio.ALL_DATA, null);
        if (jsonUsuarioString != null) {
            ObjectMapper mapper = new ObjectMapper();
            ObjectReader reader = mapper.reader(new TypeReference<Usuario>() {
            });
            Usuario usuario = reader.readValue(jsonUsuarioString);
            return usuario;
        }
        return null;

    }

    public Usuario(String email, String password, int tipo) {
        this.email = email;
        this.password = password;
        this.tipo = tipo;
    }

    public Usuario(int id, String nombre, String email, String password, String nit, String ciudad, String logo, int idUsuario, int tipo) {
        this.id = id;
        this.nombre = nombre;
        this.email = email;
        this.password = password;
        this.nit = nit;
        this.ciudad = ciudad;
        this.logo = logo;
        this.idUsuario = idUsuario;
        this.tipo = tipo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String getNit() {
        return nit;
    }

    public String getCiudad() {
        return ciudad;
    }

    public String getLogo() {
        return logo;
    }

    public int getIdUsuario() {
        return idUsuario;
    }
}

package com.antifraudminds.mindanatifraud;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;

import com.antifraudminds.mindanatifraud.activities.CamDocumentActivity;
import com.antifraudminds.mindanatifraud.activities.DenunciaActivity;
import com.antifraudminds.mindanatifraud.activities.LoginActivity;
import com.antifraudminds.mindanatifraud.activities.ValidarActivity;
import com.antifraudminds.mindanatifraud.baseactivity.AbstractFingerPrintReaderActivity;
import com.antifraudminds.mindanatifraud.fragment.FormularioFragment;
import com.antifraudminds.mindanatifraud.fragment.HomeFragment;
import com.antifraudminds.mindanatifraud.fragment.ValidarMenuFragment;
import com.antifraudminds.mindanatifraud.model.BTFingerImageReader;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.io.IOException;
import java.util.List;

public class MainActivity extends AppCompatActivity implements HomeFragment.HomeFragmentActions {



    public static final String CURRENT_FRAGMENT = "mContent";

    public enum FragmentNavigationManager {

        HOME_FRAGMENT(HomeFragment.class, "HomeFragment"),
        VALIDAR_MENU_FRAGMENT(ValidarMenuFragment.class, "ValidarMenuFragment"),
        FORMULARIO_FRAGMENT(FormularioFragment.class, "FormularioFragment");

        private Class<? extends Fragment> fragmentClass;
        private String tagName;

        FragmentNavigationManager(Class<? extends Fragment> fragmentClass, String tagName) {

            this.fragmentClass = fragmentClass;
            this.tagName = tagName;
        }

        public Class<? extends Fragment> getFragmentClass() {
            return fragmentClass;
        }

        public String getTagName() {
            return tagName;
        }

        public static FragmentNavigationManager getFragment(Class<? extends Fragment> fragmentClass) {
            for (FragmentNavigationManager fragmentNavigationManager : FragmentNavigationManager.values()) {
                if (fragmentNavigationManager.fragmentClass.isInstance(fragmentClass)) {
                    return fragmentNavigationManager;
                }
            }
            return HOME_FRAGMENT;
        }
    }

    public static final int TAKE_PICTURE = 13;
    private Fragment currentFragment;

    public interface CedulaScan {
        void parseResult(String result);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (currentFragment != null) {
            FragmentNavigationManager fragmentNavigationManager = FragmentNavigationManager.getFragment(currentFragment.getClass());
            performFragmentTransaction(fragmentNavigationManager, currentFragment, true);
        } else {
            addFragment(FragmentNavigationManager.HOME_FRAGMENT);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
      return true;
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);

        getSupportFragmentManager().putFragment(outState, CURRENT_FRAGMENT, currentFragment);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState != null) {
            //Restore the fragment's instance
            currentFragment = getSupportFragmentManager().getFragment(savedInstanceState, CURRENT_FRAGMENT);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (getSupportFragmentManager().getBackStackEntryCount() <= 0) {
            finish();
        }
    }

    private void addFragment(FragmentNavigationManager fragmentNavigationManager) {
        try {
            Fragment fragment = fragmentNavigationManager.fragmentClass.newInstance();
            currentFragment = fragment;
            performFragmentTransaction(fragmentNavigationManager, fragment, false);
        } catch (Exception exp) {

        }
    }

    private void replaceFragment(FragmentNavigationManager fragmentNavigationManager) {
        try {
            Fragment fragment = fragmentNavigationManager.fragmentClass.newInstance();
            currentFragment = fragment;
            performFragmentTransaction(fragmentNavigationManager, fragment, true);
        } catch (Exception exp) {

        }
    }

    private void performFragmentTransaction(FragmentNavigationManager fragmentNavigationManager, Fragment fragment, boolean isReplace) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        if (!isReplace) {
            transaction.add(R.id.main_content, fragment, fragmentNavigationManager.tagName);
        } else {
            transaction.replace(R.id.main_content, fragment, fragmentNavigationManager.tagName);
        }
        transaction.addToBackStack(fragmentNavigationManager.tagName);
        transaction.commit();
        getSupportFragmentManager().executePendingTransactions();
    }




    //region HomeFragment Actions

    @Override
    public void abrirValidar() throws IOException {

        if (LoginActivity.isLogued(this)) {
            Intent intentValidar = new Intent(this, ValidarActivity.class);
            startActivity(intentValidar);
        } else {
            Intent intentLogin = new Intent(this, LoginActivity.class);
            intentLogin.putExtra(LoginActivity.KEY_NEXT_SCREEN, LoginActivity.NEXT_SCREENS.VALIDAR.ordinal());
            intentLogin.putExtra(LoginActivity.KEY_LOGIN_TYPE, LoginActivity.LOGIN_TYPE.NORMAL.ordinal());
            startActivity(intentLogin);
        }
    }

    @Override
    public void abrirDenunciar() {
        Intent intentLogin = new Intent(this, LoginActivity.class);
        intentLogin.putExtra(LoginActivity.KEY_NEXT_SCREEN, LoginActivity.NEXT_SCREENS.DENUNCIA.ordinal());
        intentLogin.putExtra(LoginActivity.KEY_LOGIN_TYPE, LoginActivity.LOGIN_TYPE.QRCODE.ordinal());
        startActivity(intentLogin);
    }
    //endregion

    private BTFingerImageReader.FingerImageReaderListener getCurrentFingerImageReaderInstance() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        BTFingerImageReader.FingerImageReaderListener currentInstance = null;
        for (Fragment fragment : fragments) {
            if (fragment != null && fragment instanceof BTFingerImageReader.FingerImageReaderListener) {
                currentInstance = ((BTFingerImageReader.FingerImageReaderListener) fragment);
                break;
            } else if (currentFragment != null && currentFragment instanceof BTFingerImageReader.FingerImageReaderListener){
                currentInstance = ((BTFingerImageReader.FingerImageReaderListener) fragment);
                break;
            }
        }
        return currentInstance;
    }

    private CedulaScan getCurrentCedulaScanInstance() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        CedulaScan currentInstance = null;
        for (Fragment fragment : fragments) {
            if (fragment != null && fragment instanceof CedulaScan) {
                currentInstance = ((CedulaScan) fragment);
                break;
            } else if (currentFragment != null && currentFragment instanceof CedulaScan){
                currentInstance = ((CedulaScan) fragment);
                break;
            }
        }
        return currentInstance;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_OK) {

        }

        if ((requestCode & 0xFFFF) == IntentIntegrator.REQUEST_CODE) {
            IntentResult scanResult = IntentIntegrator.parseActivityResult((requestCode & 0xFFFF), resultCode, data);
            String content = scanResult.getContents();
            getCurrentCedulaScanInstance().parseResult(content);
        }
    }

}

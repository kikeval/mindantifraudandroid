package com.antifraudminds.mindanatifraud.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Toast;

import com.antifraudminds.mindanatifraud.R;
import com.antifraudminds.mindanatifraud.activities.CamDocumentActivity;
import com.antifraudminds.mindanatifraud.activities.ValidarActivity;
import com.antifraudminds.mindanatifraud.model.BTFingerImageReader;
import com.antifraudminds.mindanatifraud.model.DecrimData;
import com.antifraudminds.mindanatifraud.model.DecrimDataArchivos;
import com.antifraudminds.mindanatifraud.model.DecrimResponseEnvelope;
import com.antifraudminds.mindanatifraud.model.HuellaResponseData;
import com.antifraudminds.mindanatifraud.model.Licencia;
import com.antifraudminds.mindanatifraud.model.PerfilPersona;
import com.antifraudminds.mindanatifraud.model.ResponseManager;
import com.antifraudminds.mindanatifraud.model.ResponseManagerLicencia;
import com.antifraudminds.mindanatifraud.model.Usuario;
import com.antifraudminds.mindanatifraud.service.BluetoothReaderService;
import com.antifraudminds.mindanatifraud.views.LoadingDialog;
import com.google.zxing.integration.android.IntentIntegrator;
import com.microblink.activity.Pdf417ScanActivity;
import com.microblink.recognizers.blinkbarcode.pdf417.Pdf417RecognizerSettings;
import com.microblink.recognizers.settings.RecognitionSettings;
import com.microblink.recognizers.settings.RecognizerSettings;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class FormularioFragment extends Fragment implements BTFingerImageReader.FingerImageReaderListener {


    public static final String FOTO_TIRO_CEDULA = "fotoTiroCedula";
    public static final String FOTO_RETIRO_CEDULA = "fotoRetiroCedula";
    public static final String FOTO_USUARIO = "fotoUsuario";
    public static final String HUELLA_RESPONSE = "huellaResponse";
    public static final String FIRMA_USUARIO = "firmaUsuario";
    public static final int SCAN_CEDULA = 23344;
    public static String LICENSE_KEY_MOBI = "47FYV4FO-EVAIGOHG-NYJYTQHS-2VOSZFEG-WENBORB5-634FUOMJ-YBZN7WM3-P7DRPYRJ";

    public interface FormularioFragmentActions {
        void tomarFotoCedula();
        void verTerminos();
        void tomarFoto();
        void pedirFirma();
    }
    private BTFingerImageReader.FingerImageReaderListener listener;
    private PerfilPersona cedulaData;
    private FormularioFragmentActions actions;

    public enum FingerState{
        CONNECT,
        CAPTURE;
    }

    //region Controles
    @BindView(R.id.main_scroll_view)
    ScrollView mainScrollView;

    @BindView(R.id.huella)
    Button btnHuella;
    @BindView(R.id.cedula)
    Button btnCedula;
    @BindView(R.id.btnEscanearDocumento)
    Button btnEscanearDocumento;
    @BindView(R.id.fotoBtn)
    Button fotoBtn;

    @BindView(R.id.txtNombres)
    EditText txtNombres;
    @BindView(R.id.txtApellidos)
    EditText txtApellidos;
    @BindView(R.id.txtTipoDocumento)
    EditText txtTipoDocumento;
    @BindView(R.id.txtNumeroDocumento)
    EditText txtNumeroDocumento;
    @BindView(R.id.txtSexo)
    EditText txtSexo;
    @BindView(R.id.txtFechaNacimiento)
    EditText txtFechaNacimiento;
    @BindView(R.id.txtRH)
    EditText txtRH;
    @BindView(R.id.txtDescripcion)
    EditText txtDescripcion;

    @BindView(R.id.imgDocumentoValidar)
    CircularImageView documentoValidar;
    @BindView(R.id.imgHuellaValidar)
    CircularImageView huellaValidar;
    @BindView(R.id.imgUserValidar)
    CircularImageView usuarioValidar;

    @BindView(R.id.validacionImagenUsuario)
    ImageView validacionImagenUsuario;
    @BindView(R.id.validacionImagenHuella)
    ImageView validacionImagenHuella;
    @BindView(R.id.validacionImagenCedula)
    ImageView validacionImagenCedula;

    @BindView(R.id.btnFirmaUsuario)
    Button btnFirmaUsuario;
    @BindView(R.id.firmaUsuario)
    ImageView firmaUsuario;

    @BindView(R.id.terminosBtn)
    Button btnTerminos;

    @BindView(R.id.btnSendValidar)
    Button btnSendValidar;

    //endregion

    //private Bitmap fotoUsuario;
    //private Bitmap fotoTiroCedula;
    //private Bitmap fotoRetiroCedula;
    //private Bitmap firma;

    //private Bitmap fotoUsuario;
    //private Bitmap fotoTiroCedula;
    //private Bitmap fotoRetiroCedula;
    private String firma;
    private HuellaResponseData huellaResponseData;

    private String fotoUsuarioFile;
    private String fotoTiroCedulaFile;
    private String fotoRetiroCedulaFile;

    public FormularioFragment (){

    }

    //region Ciclo de Vida del Fragment


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.formulario_fragment, container, false);
        ButterKnife.bind(this, rootView);
        setListener();
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getActivity() instanceof BTFingerImageReader.FingerImageReaderListener) {
            listener = (BTFingerImageReader.FingerImageReaderListener) getActivity();
        }

        if (getActivity() instanceof FormularioFragmentActions) {
            actions = (FormularioFragmentActions) getActivity();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Licencia.getLicencia(new Callback<ResponseManagerLicencia>() {
            @Override
            public void onResponse(Call<ResponseManagerLicencia> call, Response<ResponseManagerLicencia> response) {
                if (response != null && response.body() != null && response.body().getObject() != null) {
                    LICENSE_KEY_MOBI = response.body().getObject().getLicense();
                }
            }

            @Override
            public void onFailure(Call<ResponseManagerLicencia> call, Throwable t) {

            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(FIRMA_USUARIO, firma);
        outState.putString(FOTO_TIRO_CEDULA, fotoTiroCedulaFile);
        outState.putString(FOTO_RETIRO_CEDULA, fotoRetiroCedulaFile);
        outState.putString(FOTO_USUARIO, fotoUsuarioFile);
        outState.putParcelable(HUELLA_RESPONSE, huellaResponseData);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(FOTO_TIRO_CEDULA)) {
                fotoTiroCedulaFile = savedInstanceState.getString(FOTO_TIRO_CEDULA);
                if (fotoTiroCedulaFile != null) {
                    try {
                        documentoValidar.setImageBitmap(ValidarActivity.getScaledTo(fotoTiroCedulaFile, 200, 200));
                        validacionImagenCedula.setImageResource(R.drawable.aprovaded);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (savedInstanceState.containsKey(FOTO_RETIRO_CEDULA)) {
                fotoRetiroCedulaFile = savedInstanceState.getString(FOTO_RETIRO_CEDULA);
            }
            if (savedInstanceState.containsKey(FOTO_USUARIO)) {
                fotoUsuarioFile = savedInstanceState.getString(FOTO_USUARIO);
                if (fotoUsuarioFile != null) {
                    try {
                        usuarioValidar.setImageBitmap(fixPhotoResult(fotoUsuarioFile, true));
                        validacionImagenUsuario.setImageResource(R.drawable.aprovaded);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (savedInstanceState.containsKey(HUELLA_RESPONSE)) {
                huellaResponseData = savedInstanceState.getParcelable(HUELLA_RESPONSE);
                if (huellaResponseData != null && huellaResponseData.getHuellaBitmap() != null) {
                    huellaValidar.setImageBitmap(huellaResponseData.getHuellaBitmap());
                    validacionImagenHuella.setImageResource(R.drawable.aprovaded);
                }
            }
            if (savedInstanceState.containsKey(FIRMA_USUARIO)) {
                firma = savedInstanceState.getString(FIRMA_USUARIO);
                if (firma != null) {
                    firmaUsuario.setImageBitmap(BitmapFactory.decodeFile(firma));
                }
            }
        }
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (IntentIntegrator.REQUEST_CODE == requestCode) {

        }
    }
    //endregion

    //region Metodos privados

    private void setListener() {
        btnHuella.setTag(FingerState.CONNECT);
        btnHuella.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FingerState state = (FingerState) view.getTag();
                switch (state) {
                    case CONNECT:
                        ((ValidarActivity) getActivity()).initialize();
                        ((ValidarActivity) getActivity()).connect();
                        break;
                    case CAPTURE:
                        ((ValidarActivity) getActivity()).capture();
                        break;
                }
            }
        });

        btnCedula.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (actions != null) {
                    actions.tomarFotoCedula();
                }
            }
        });
        btnEscanearDocumento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                escanearDocumento();
            }
        });

        fotoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (actions != null) {
                    actions.tomarFoto();
                }
            }
        });

        documentoValidar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fotoTiroCedulaFile != null) {
                    try {
                        documentoValidar.setImageBitmap(ValidarActivity.getScaledTo(fotoTiroCedulaFile, 200, 200));
                        documentoValidar.invalidate();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        btnFirmaUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestFirma();
            }
        });
        btnTerminos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (actions != null) {
                    actions.verTerminos();
                }
            }
        });

        btnSendValidar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendValidar();
            }
        });
    }

    private boolean informacionEsValida() {
        try {
            boolean esValido = true;
            esValido &= !txtNombres.getText().toString().isEmpty();
            esValido &= !txtApellidos.getText().toString().isEmpty();
            esValido &= !txtTipoDocumento.getText().toString().isEmpty();
            esValido &= !txtNumeroDocumento.getText().toString().isEmpty();
            esValido &= !txtFechaNacimiento.getText().toString().isEmpty();
            esValido &= !txtSexo.getText().toString().isEmpty();
            esValido &= !txtRH.getText().toString().isEmpty();
            esValido &= fotoTiroCedulaFile != null;
            esValido &= fotoUsuarioFile != null;
            esValido &= firma != null;
            esValido &= huellaResponseData != null && huellaResponseData.getHuellaBitmap() != null && huellaResponseData.getData() != null;
            return esValido;
        } catch (Exception exp) {
            LoadingDialog.updateText("Error:" + exp.getMessage());
            LoadingDialog.showDialog(this.getActivity());
            return false;
        }
    }

    private void sendValidar() {
        if (informacionEsValida()) {
            try {
                LoadingDialog.showDialogNoCancelable(getActivity());
                LoadingDialog.updateText("Enviando Datos Básicos");
                final PerfilPersona perfilPersona = new PerfilPersona();
                perfilPersona.setNombres(txtNombres.getText().toString());
                perfilPersona.setApellidos(txtApellidos.getText().toString());
                perfilPersona.setSexo(txtSexo.getText().toString());
                perfilPersona.setFechaNacimiento(txtFechaNacimiento.getText().toString());
                perfilPersona.setHuellaResponseData(huellaResponseData);
                perfilPersona.setNumeroDocumento(txtNumeroDocumento.getText().toString());
                perfilPersona.setRHSanguineo(txtRH.getText().toString());
                perfilPersona.setTipoDocumento(txtTipoDocumento.getText().toString());
                perfilPersona.setObservaciones(txtDescripcion.getText().toString());
                PerfilPersona.enviarDatos(perfilPersona, new Callback<DecrimResponseEnvelope>() {
                    @Override
                    public void onResponse(Call<DecrimResponseEnvelope> call, Response<DecrimResponseEnvelope> response) {
                        LoadingDialog.updateText("Subiendo imagenes");

                        if (response.errorBody() != null) {
                            try {
                                LoadingDialog.updateText("Error envio a Decrim:" + response.errorBody().string());
                                LoadingDialog.showDialog(getActivity());
                                Toast.makeText(getContext(), response.errorBody().string(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                        if (response.raw() != null) {
                            try {
                                String responseData = response.body().getBody().getInsertCasoResponse().getInsertCasoResult().get(1);
                                if (responseData.split(":").length > 1) {
                                    responseData = responseData.split(":")[1].trim();
                                    perfilPersona.setIdCaso(Integer.parseInt(responseData));
                                    Toast.makeText(getContext(), "Insertado id caso:" + responseData, Toast.LENGTH_LONG).show();
                                    PerfilPersona.sendInsertImgCaso(perfilPersona.getIdCaso(), new ArrayList<Bitmap>() {{
                                        add(ValidarActivity.getBitmapFromFile(fotoTiroCedulaFile));
                                        add(ValidarActivity.getBitmapFromFile(fotoRetiroCedulaFile));
                                        add(fixPhotoResult(fotoUsuarioFile, false));
                                        //add(firma);
                                        add(huellaResponseData.getHuellaBitmap());
                                    }}, new Drawable.Callback() {
                                        @Override
                                        public void invalidateDrawable(Drawable who) {
                                            registrarCasoEnAntiFraudminds(perfilPersona);


                                        }

                                        @Override
                                        public void scheduleDrawable(Drawable who, Runnable what, long when) {

                                        }

                                        @Override
                                        public void unscheduleDrawable(Drawable who, Runnable what) {

                                        }
                                    });
                                } else {
                                    LoadingDialog.updateText("respuesta decrim falló:" + responseData);
                                    LoadingDialog.showDialog(FormularioFragment.this.getActivity());
                                }
                            } catch (Exception e) {
                                Toast.makeText(getContext(), "Error leyendo:" + e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<DecrimResponseEnvelope> call, Throwable t) {
                        Toast.makeText(getContext(), "Hay una falla:" + call.isExecuted(), Toast.LENGTH_LONG).show();
                    }
                });
            } catch (Exception exp) {
                LoadingDialog.updateText("Error en Envio Try General:" + exp.getMessage());
                LoadingDialog.showDialog(this.getActivity());
            }
        } else {
            Toast.makeText(getContext(), "La información no esta completa, revisela e intente de nuevo", Toast.LENGTH_LONG).show();
        }

    }

    private void registrarCasoEnAntiFraudminds(final PerfilPersona perfilPersona) {
        LoadingDialog.updateText("Registrando Caso");

        Usuario usuario = null;
        try {
            usuario = Usuario.retrieveUsuarioLoginData(getActivity().getSharedPreferences(Usuario.KEY_USUARIO,
                    MODE_PRIVATE));
        } catch (IOException e) {
            e.printStackTrace();
        }

        DecrimData decrimData = new DecrimData();
        decrimData.setIdUsuario(usuario.getIdUsuario());
        decrimData.setIdEmpresa(usuario.getId());
        decrimData.setIdCaso(perfilPersona.getIdCaso());
        decrimData.setNombres(perfilPersona.getNombres());
        decrimData.setApellidos(perfilPersona.getApellidos());
        decrimData.setNumDocumento(perfilPersona.getNumeroDocumento());
        decrimData.setRh(perfilPersona.getRHSanguineo());
        decrimData.setSexo(perfilPersona.getSexo());
        try {
            decrimData.setFoto(PerfilPersona.getImageBase64(fixPhotoResult(fotoUsuarioFile, false)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        decrimData.setFechaNacimiento(perfilPersona.getFechaNacimiento());
        DecrimData.sendDecrimData(decrimData, new Callback<ResponseManager>() {
            @Override
            public void onResponse(Call<ResponseManager> call, Response<ResponseManager> response) {
                LoadingDialog.updateText("Subiendo imagenes Caso");
                sendArchivos(perfilPersona.getIdCaso(), 0);
            }

            @Override
            public void onFailure(Call<ResponseManager> call, Throwable t) {

            }
        });
    }

    private Bitmap getFileToSend(int index) throws FileNotFoundException {
        switch (index) {
            case 0:
                return huellaResponseData.getHuellaBitmap();
            case 1:
                return ValidarActivity.getBitmapFromFile(fotoTiroCedulaFile);
            case 2:
                return ValidarActivity.getBitmapFromFile(fotoRetiroCedulaFile);
            case 3:
                return BitmapFactory.decodeFile(firma);
        }
        try {
            return fixPhotoResult(fotoUsuarioFile, false);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void sendArchivos(final int idCaso, final int numArchivosSent) {
        final String [] archivosNombres = {"Huella", "Cedula Adelante", "Cedula Atras", "UFirma"};
        if (numArchivosSent < archivosNombres.length) {
            DecrimDataArchivos archivo = new DecrimDataArchivos();
            archivo.setIdCaso(idCaso);
            archivo.setNombre(archivosNombres[numArchivosSent]);
            try {
                archivo.setArchivoUrl(PerfilPersona.getImageBase64(getFileToSend(numArchivosSent)));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            DecrimDataArchivos.sendDecrimDataArchivo(archivo, new Callback<ResponseManager>() {
                @Override
                public void onResponse(Call<ResponseManager> call, Response<ResponseManager> response) {
                    if (numArchivosSent < archivosNombres.length) {
                        sendArchivos(idCaso, numArchivosSent + 1);
                    } else {
                        //getActivity().finish();
                    }
                }

                @Override
                public void onFailure(Call<ResponseManager> call, Throwable t) {

                }
            });
        } else {
            LoadingDialog.closeDialog();
            getActivity().finish();
        }
    }


    private void requestFirma() {
        if (actions != null) {
            actions.pedirFirma();
        }
    }

    private void escanearDocumento() {
        // Intent for Pdf417ScanActivity Activity
        Intent intent = new Intent(getActivity(), Pdf417ScanActivity.class);

        // set your licence key
        // obtain your licence key at http://microblink.com/login or
        // contact us at http://help.microblink.com
        intent.putExtra(Pdf417ScanActivity.EXTRAS_LICENSE_KEY, LICENSE_KEY_MOBI);

        RecognitionSettings settings = new RecognitionSettings();
        // setup array of recognition settings (described in chapter "Recognition
        // settings and results")
        settings.setRecognizerSettingsArray(setupSettingsArray());
        intent.putExtra(Pdf417ScanActivity.EXTRAS_RECOGNITION_SETTINGS, settings);
        intent.putExtra(Pdf417ScanActivity.EXTRAS_SHOW_DIALOG_AFTER_SCAN, false);

        // Starting Activity
        startActivityForResult(intent, SCAN_CEDULA);
    }

    private RecognizerSettings[] setupSettingsArray() {
        Pdf417RecognizerSettings sett = new Pdf417RecognizerSettings();
        // disable scanning of white barcodes on black background
        sett.setInverseScanning(false);
        // allow scanning of barcodes that have invalid checksum
        sett.setUncertainScanning(true);
        // disable scanning of barcodes that do not have quiet zone
        // as defined by the standard
        sett.setNullQuietZoneAllowed(false);

        // now add sett to recognizer settings array that is used to configure
        // recognition
        return new RecognizerSettings[] { sett };
    }

    private void fillScanCedulaData(PerfilPersona cedulaData) {
        txtNombres.setText(cedulaData.getNombres());
        txtApellidos.setText(cedulaData.getApellidos());
        txtNumeroDocumento.setText(cedulaData.getNumeroDocumento());
        txtTipoDocumento.setText("CC");
        txtRH.setText(cedulaData.getRHSanguineo());
        txtFechaNacimiento.setText(cedulaData.getFechaNacimiento());
        txtSexo.setText(cedulaData.getSexo());
    }

    //endregion

    //region Public Methods
    public void setImageDocumentoValidar(String fotoTiroCedulaFile, String fotoRetiroCedulaFile) {

        this.fotoTiroCedulaFile = fotoTiroCedulaFile;
        this.fotoRetiroCedulaFile = fotoRetiroCedulaFile;
        try {
            Bitmap fotoTiroCedula = ValidarActivity.getScaledTo(fotoTiroCedulaFile, 200, 200);
            documentoValidar.setImageBitmap(fotoTiroCedula);
            documentoValidar.setVisibility(View.VISIBLE);
            documentoValidar.invalidate();
            validacionImagenCedula.setImageResource(R.drawable.aprovaded);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    private Bitmap fixPhotoResult(String photoPath, boolean miniImage) throws IOException {
        ExifInterface ei = new ExifInterface(CamDocumentActivity.getFileUri(photoPath).getPath());
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);

        Bitmap bitmap = miniImage ? ValidarActivity.getScaledTo(photoPath, 200, 200) : ValidarActivity.getBitmapFromFile(photoPath);
        Bitmap rotatedBitmap = null;
        switch(orientation) {

            case ExifInterface.ORIENTATION_ROTATE_90:
                rotatedBitmap = rotateImage(bitmap, 90);
                break;

            case ExifInterface.ORIENTATION_ROTATE_180:
                rotatedBitmap = rotateImage(bitmap, 180);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
                rotatedBitmap = rotateImage(bitmap, 270);
                break;

            case ExifInterface.ORIENTATION_NORMAL:
                rotatedBitmap = bitmap;
                break;

            default:
                rotatedBitmap = bitmap;
                break;
        }
        return rotatedBitmap;
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    public void setImageUsuario(String fotoUsuarioFile) {
        this.fotoUsuarioFile = fotoUsuarioFile;
        try {
            Bitmap fotoUsuario = fixPhotoResult(fotoUsuarioFile, true);
            usuarioValidar.setImageBitmap(fotoUsuario);
            usuarioValidar.setVisibility(View.VISIBLE);
            validacionImagenUsuario.setImageResource(R.drawable.aprovaded);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setImageFirma(String firma) {
        this.firma = firma;
        if (firma != null) {
            firmaUsuario.setImageBitmap(BitmapFactory.decodeFile(firma));
            firmaUsuario.setVisibility(View.VISIBLE);
        }
    }
    //endregion

    //region Métodos de {@link FingerImageReaderListener}
    @Override
    public void stateChanged(int state) {
        switch (state) {
            case BluetoothReaderService.STATE_NONE:
            case BluetoothReaderService.STATE_LISTEN:
                btnHuella.setTag(FingerState.CONNECT);
                btnHuella.setBackgroundResource(R.drawable.connect);
                break;
            case BluetoothReaderService.STATE_CONNECTED:
                btnHuella.setTag(FingerState.CAPTURE);
                btnHuella.setBackgroundResource(R.drawable.huella);
                break;
        }
    }

    @Override
    public void imageCaptured(HuellaResponseData data) {
        huellaResponseData = data;
        huellaValidar.setImageBitmap(data.getHuellaBitmap());
        huellaValidar.setVisibility(View.VISIBLE);
        validacionImagenHuella.setImageResource(R.drawable.aprovaded);
    }
    //endregion

    //region Metodos de {@link CedulaScan}
    public void parseResult(String result) {
        try {
            cedulaData = PerfilPersona.parseCedulaInfo(result);
            if (cedulaData != null) {
                fillScanCedulaData(cedulaData);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }
    //endregion




}
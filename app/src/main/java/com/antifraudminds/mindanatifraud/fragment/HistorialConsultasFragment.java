package com.antifraudminds.mindanatifraud.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.antifraudminds.mindanatifraud.R;
import com.antifraudminds.mindanatifraud.adapter.HistoryValidateAdapter;
import com.antifraudminds.mindanatifraud.model.HistoryValidateObject;

import java.util.ArrayList;

public class HistorialConsultasFragment extends Fragment implements ListView.OnItemClickListener {

   public HistorialConsultasFragment(){
   }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //inflate fragment
        View rootView = inflater.inflate(R.layout.fragment3,container, false);

        //create adapter
        ListView listView = (ListView) rootView.findViewById(R.id.listData);

        ArrayList<HistoryValidateObject> subjects = new ArrayList<HistoryValidateObject>();

        //bin adapter
        HistoryValidateAdapter adapter = new HistoryValidateAdapter(getContext(),R.layout.rowlayout, subjects);

        listView.setAdapter(adapter);

        return (rootView);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    }
}
package com.antifraudminds.mindanatifraud.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.antifraudminds.mindanatifraud.R;
import com.antifraudminds.mindanatifraud.model.Banner;
import com.antifraudminds.mindanatifraud.model.ResponseManagerBanner;
import com.antifraudminds.mindanatifraud.model.ServiceCommunication;
import com.antifraudminds.mindanatifraud.views.LoadingDialog;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment {

    public static final String BANNERS = "banners";
    @BindView(R.id.btnValidar)
    Button btnValidar;
    @BindView(R.id.btnDenunciar)
    Button btnDenunciar;

    @BindView(R.id.banners)
    LinearLayout bannersContainer;

    private View rootView;

    private HomeFragmentActions actions;
    private List<Banner> banners;


    public interface HomeFragmentActions {
        void abrirValidar() throws IOException;
        void abrirDenunciar();
    }

    public HomeFragment(){

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getActivity() instanceof HomeFragmentActions) {
            actions = (HomeFragmentActions) getActivity();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.home_fragment, container, false);
        ButterKnife.bind(this, rootView);
        setListeners();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        setBanners();
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(BANNERS)) {
                banners = (List<Banner>) savedInstanceState.getSerializable(BANNERS);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(BANNERS, (Serializable) banners);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    private void setListeners() {
        btnValidar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (actions != null) {
                    try {
                        actions.abrirValidar();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        btnDenunciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (actions != null) {
                    actions.abrirDenunciar();
                }
            }
        });
    }

    private void setBanners() {
        if (banners == null) {
            LoadingDialog.showDialog(getActivity());
            LoadingDialog.updateText("");
            Banner.getBanners(new Callback<ResponseManagerBanner>() {
                @Override
                public void onResponse(Call<ResponseManagerBanner> call, Response<ResponseManagerBanner> response) {
                    LoadingDialog.closeDialog();
                    ResponseManagerBanner responseManagerBanner = response.body();
                    if (responseManagerBanner != null) {
                        banners = responseManagerBanner.getObject();
                        fillBannersIntoImages();
                    }
                }

                @Override
                public void onFailure(Call<ResponseManagerBanner> call, Throwable t) {
                    LoadingDialog.closeDialog();
                }
            });
        } else {
            fillBannersIntoImages();
        }
    }

    private ImageView createImageView() {
        /*
        <ImageView
                    android:id="@+id/banner1"
                    android:layout_width="80dp"
                    android:layout_height="80dp"
                    android:layout_gravity="center"
                    android:layout_margin="20dp"
                    android:padding="10dp"/>
         */
        ImageView imageView = new ImageView(this.getActivity());
        int size = getActivity().getResources().getDimensionPixelOffset(R.dimen.size_banner);
        imageView.setMaxWidth(size);
        imageView.setMinimumWidth(size);
        imageView.setMaxHeight(size);
        imageView.setMinimumHeight(size);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(size, size);
        int margin = getActivity().getResources().getDimensionPixelSize(R.dimen.margin_banner);
        layoutParams.setMargins(margin, margin, margin, margin);
        imageView.setLayoutParams(layoutParams);
        return imageView;

    }

    private void fillBannersIntoImages() {
        bannersContainer.removeAllViews();
        for (int i = 0; i < banners.size(); i++) {
            Banner banner = banners.get(i);
            View imageBanner = createImageView();
            bannersContainer.addView(imageBanner);
            Picasso.with(getActivity()).load(ServiceCommunication.BASE_URL + banner.getUrlImage()).into((ImageView) imageBanner);
            if (banner.getUrl() != null) {
                imageBanner.setTag(banner.getUrl());
                imageBanner.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String url = (String) view.getTag();
                        Intent intentDoBrowser = new Intent(Intent.ACTION_VIEW);
                        intentDoBrowser.setData(Uri.parse(url));
                        startActivity(intentDoBrowser);
                    }
                });
            }
        }
    }

}
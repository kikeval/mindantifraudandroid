package com.antifraudminds.mindanatifraud.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.antifraudminds.mindanatifraud.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ValidarMenuFragment extends Fragment {

    @BindView(R.id.btnVerHistorial)
    Button btnVerHistorial;
    @BindView(R.id.btnValidar)
    Button btnValidar;

    private ValidarMenuFragmentActions actions;

    public interface ValidarMenuFragmentActions {
        void verHistorial();
        void validar();
    }

    public ValidarMenuFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity = getActivity();
        if (activity instanceof ValidarMenuFragmentActions) {
            actions = (ValidarMenuFragmentActions) activity;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_validar_menu, container, false);
        ButterKnife.bind(this, rootView);
        setListeners();
        return rootView;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void setListeners() {
        btnVerHistorial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (actions != null) {
                    actions.verHistorial();
                }
            }
        });

        btnValidar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (actions != null) {
                    actions.validar();
                }
            }
        });
    }
}

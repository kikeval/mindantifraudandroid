package com.antifraudminds.mindanatifraud.baseactivity;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.antifraudminds.mindanatifraud.R;
import com.antifraudminds.mindanatifraud.activities.DeviceListActivity;
import com.antifraudminds.mindanatifraud.model.BTFingerImageReader;
import com.antifraudminds.mindanatifraud.service.BluetoothReaderService;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Clase que define un activity que maneja un Bluetooth Finger Print Reader.
 *
 * @author gustavo.bonilla
 */
public abstract class AbstractFingerPrintReaderActivity extends AppCompatActivity implements BTFingerImageReader.FingerImageReaderListener {

    //region Constantes de la Clase
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    //endregion

    //region Attributos Privados
    private BluetoothAdapter mBluetoothAdapter = null;
    private BTFingerImageReader fingerImageReader;
    //endregion

    //region Métodos del Ciclo de Vida del Activity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            //finish();
            return;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    // Get the device MAC address
                    String address = data.getExtras()
                                         .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);

                    if (validateMacAddress(address)) {
                        // Get the BLuetoothDevice object
                        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
                        // Attempt to connect to the device
                        if (fingerImageReader == null) {
                            fingerImageReader = new BTFingerImageReader(this, this);
                        }
                        fingerImageReader.connect(device);
                    }
                }
                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a chat session
                    if (fingerImageReader == null) {
                       fingerImageReader = new BTFingerImageReader(this, this);
                    }
                    fingerImageReader.setBTService();
                    int serviceState = fingerImageReader.getBTState();
                    if (serviceState != -1 && (serviceState == BluetoothReaderService.STATE_NONE || serviceState == BluetoothReaderService
                            .STATE_LISTEN)) {
                        setRequestConnectDevice();
                    }

                } else {
                    // User did not enable Bluetooth or an error occured
                    Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
                    finish();
                }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Stop the Bluetooth chat services
        if (fingerImageReader != null) {
            fingerImageReader.stopService();
        }
    }
    //endregion

    //region Métodos Privados

    private boolean validateMacAddress(String macAddress) {
        String pattern = "^([0-9A-F]{2}[:-]){5}([0-9A-F]{2})$";
        Pattern patternObject = Pattern.compile(pattern);
        Matcher matcher = patternObject.matcher(macAddress);
        return matcher.find();
    }
    private void ensureDiscoverable() {
        if (mBluetoothAdapter != null && mBluetoothAdapter.getScanMode() != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            startActivity(discoverableIntent);
        }
    }
    //endregion

    //region Métodos Protegidos

    public void initialize() {
        // If BT is not on, request that it be enabled.
        // setupChat() will then be called during onActivityResult
        if (mBluetoothAdapter != null) {
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
                // Otherwise, setup the chat session
            } else {
                if (fingerImageReader == null) {
                    fingerImageReader = new BTFingerImageReader(this, this);
                    fingerImageReader.setBTService();
                }
            }
        }
    }

    protected void setRequestConnectDevice() {
        ensureDiscoverable();
        Intent serverIntent = new Intent(this, DeviceListActivity.class);
        startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
    }

    protected void sendCommand() {
        fingerImageReader.sendCommand();
    }

    protected int getState() {
        return fingerImageReader != null ? fingerImageReader.getBTState() : -1;
    }
    //endregion



}

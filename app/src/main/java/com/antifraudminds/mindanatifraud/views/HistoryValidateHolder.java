package com.antifraudminds.mindanatifraud.views;

import android.widget.TextView;

import com.antifraudminds.mindanatifraud.R;
import com.mikhaellopez.circularimageview.CircularImageView;

/**
 * Created by gustavo.bonilla on 8/07/2017.
 */

public class HistoryValidateHolder {

    public TextView textViewName;
    public TextView textViewCC;
    public CircularImageView imageViewStatus;
}

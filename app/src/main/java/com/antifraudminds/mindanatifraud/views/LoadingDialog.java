package com.antifraudminds.mindanatifraud.views;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by gustavo.bonilla on 12/5/2016.
 */

public class LoadingDialog {

    private static ProgressDialog currentDialog;

    public static void showDialog(Activity context) {
        currentDialog = ProgressDialog.show(context, "Antifraud Minds" , "Wait while loading...");
        currentDialog.setCancelable(true);
    }

    public static void showDialogNoCancelable(Activity context) {
        currentDialog = ProgressDialog.show(context, "Antifraud Minds" , "Wait while loading...");
        currentDialog.setCancelable(false);
    }

    public static void closeDialog() {
        if (currentDialog != null) {
            currentDialog.dismiss();
        }
    }

    public static void updateText(String text) {
        if (currentDialog != null) {
            currentDialog.setMessage(text);
        }
    }
}
